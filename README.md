# Magic Cartridge Generator

Magic Cartridge Generator is continuation of older version of [Magic Desk Cartridge Generator](https://bitbucket.org/zzarko/magic-desk-cartridge-generator/). The name was changed because of adding support for Commodore 264 series machines.

## Description ##

This program can be used to generate:

* Magic Desk compatible Commodore 64 cartridges (probably compatible with Gmod2 cartridges too)
* Gmod3 Commodore 64 cartridges
* Magic 264 cartridges for Commodore 264 range of computers
* Magic 128/Gmod2 Commodore 128 cartridges
* Commodore 128 External ROM cartridge with 32K banks

with programs that can be selected from menu:

![Generated C64 cartridge](./cartC64.png) ![Generated Plus/4 cartridge](./cartPlus4.png) ![Generated C128 cartridge](./cartC128.png)

Magic cartridge hardware is made by Marko Šolajić and can be found at:

[Magic Desk compatible C64 cartridge](https://github.com/msolajic/c64-magic-desk-512k)

[Magic 264 Cartridge](https://github.com/msolajic/c264-magic-cart)

Gmod2 Cartridge for Commodore 128 can be found at (many thanks to Daniël for help and testing):

[Gmod2 Cartridge for C128](https://www.freepascal.org/~daniel/gmod2/)

Magic Desk 128 Cartridge for Commodore 128 can be found at (many thanks to Per Bengtsson for testing):

[Magic Desk 128 (1MB)](https://github.com/RetroNynjah/Magic-Desk-128)

Magic Desk 128 Cartridge for Commodore 128 with 32k banks can be found at (many thanks to Maciej 'YTM/Elysium' Witkowiak for adding 32k support and 2MHz spreedup):

[Magic Desk 128 (512KB/1MB, 32K banks)](https://github.com/ytmytm/c128-magicdesk-external-rom)


All programs for the cartridge must be **one-file** in PRG format (first 2 bytes are the load address).
Cartridge and menu programs can be configured with CFG file (more advanced option), otherwise defaults will be used.

Cartridge can also be defined by just placing prg files inside prg directory. In that case:

* name of the file will be used as menu name
* you can make arbitrary order of files by placing N_ prefix, where N is a 1-3 digit number (prefix won't be a part of the menu name). Additionally, if you want multiple menus, you need to use 3-digit prefix in the form XYY_, where X is a menu number (1-8), and YY is placing inside menu. Numbers do not have to be successive
* programs in prg directory can also have suffix _N or _0xH where N is decimal, and H is hex number, and if present, that number will be used as run address (suffix won't be a part of the menu name)
* if there is a music file present inside prg directory (GoatTracker SID for C64 or KnaeckeTraecker BIN for Plus/4), it can be added as menu music

You can try out cartridge generation with supplied files:

```
python crtgen.py -c
```

* this will create C64 cartridge from all prg files in **prg** directory

```
python crtgen.py compilationP4
```

* this will create cartridge based on **compilationP4.cfg** file

Run crtgen.py without parameters to get the list of supported options:
```
python crtgen.py
```

For assembling C64 or Plus/4 source, [Kick Assembler](http://theweb.dk/KickAssembler/) version 5.17 or later is needed (probably works with earlier versions too, but I haven't tested).

If you want to test MD cartridge in [VICE emulator](http://vice-emu.sourceforge.net/), for C64 first convert it to crt format:
```
cartconv -t md -i compilation.bin -o compilation.crt
```
and then run x64 with cartcrt option:
```
x64 -cartcrt compilation.crt
```
(or select compilation.crt from menu after starting x64)

For Plus/4, you'll need VICE 3.6 or later:
```
xplus4 -cartmagic compilation.bin
```
For C128, you'll need VICE 3.8 or later:
```
cartconv -t md128 -i compilation.bin -o compilation.crt
```
and then run x128 with cartcrt option:
```
x128 -cartcrt compilation.crt
```

VICE emulator now has a support for many Magic cartridges (many thanks VICE team and especially to Groepaz):

* since v3.0, Magic Desk cartridge is supported with up to 1MB configurations
* since v3.6, Magic 264 cartridge is supported
* since v3.8, Magic 128 cartridge is supported, and all cartridge types were added to cartconv utility

Python code was tested on Linux and Windows, with Python 3.
C64, C128 and Plus/4 code was tested in VICE, YAPE, Z64K and on a real PAL and NTSC C64, C128 and 264 machines (not all combinations are tested on real hardware as we do not have many NTSC machines).
If you find bugs in the code, please report as bitbucket issue, or to one of our e-mails.

Menu ASM code was written in such a way to corrupt as less memory as possible (it corrupts around 40 bytes in zero page and cassette buffer at $0340), as it was initially meant to be used for tools compilations (that is the reason it uses PETSCII).

GoatTracker SID files are supported for C64/C128, and KnaeckeTraecker BIN are supported for Plus/4.
Be warned that they will be copied to $1000, thus corrupting RAM on those addresses.
If that is not desirable for your compilation (e.g. various tools for analysing RAM), do not use the music.

Make sure that your GoatTracker song only touches $FB-$FE on zeropage, or at least to not touch locations used by menu itself (current map is on the beginning of menu-c64.asm/menu-c128.asm).
Also, make sure that music's load address is indeed $1000, and that it uses $1000 for init and $1003 for playing. Otherwise, change the asm code and recompile it.

Similarly, KnaeckeTraecker song should only touch $08-$0A and be exported to work from $1000.
When saving the song for the cartridge, use **ExportBin** command, and select **Include Load Address** and **Include Player** options.

Be aware that for many PRG files that do not start at standard BASIC address ($0801 for C64, $1C01 for C128, $1001 for Plus/4) you need to provide start address.
This includes files starting at $0800 for C64, $1C00 for C128 and $1000 for Plus/4.

Contact e-mails: msolajic and zzarko at gmail

## Repository contents ##

* menu-c64.asm
    * ASM source code for C64 cartridge menu program (Magic Desk and Gmod3, switchable by #define), Kick Assembler 5.17
* menu-c128.asm
    * ASM source code for C128 VIC-II cartridge menu program, Kick Assembler 5.17
* menu-plus4.asm
    * ASM source code for Plus/4 cartridge menu program, Kick Assembler 5.17
* menu-c64.prg, menu-c64-gm3.prg, menu-c128.prg and menu-plus4.prg
    * alredy built menu programs
* Makefile
    * for **make** program, to generate all prg files
* compile.sh
    * BASH script used during development
* compilationC64.cfg
    * sample configuration file for a C64 cartridge
* compilationC64.bin, compilationC64.crt
    * generated C64 cartridge
* compilationP4.cfg
    * sample configuration file for a Plus/4 cartridge
* compilationP4.bin
    * generated Plus/4 cartridge
* compilationC128.cfg
    * sample configuration file for a C128 cartridge
* compilationC128.bin
    * generated C128 cartridge (can be executed in VICE and Z64K)
    * for now, only 16k compilations can be executed in emulators (32k can be loaded, but only first 16k will work)
* compilationC128-Games.bin
    * sample C128 cartridge with many recent games (copyrighted by their respective authors)
    * for now it can only be used on real machine, as there is no support for banking cartridges in current C128 emulators
    * Frantic Freddie II 128 for some reason does not work from cartridge, if someone can help, please do
* crtgen.py
    * Python program that links prg files with menu program and generates bin file that can be burned to eprom/flash, or in case of C64 cartridge, converted to CRT file for emulation (using VICE's cartconv)
    * it should run on any system that has Python 3 and required modules installed (see beginning of crtgen.py for required modules if you get an error about that)
* HelloWorld.sid (inside prg directory)
    * music made specifically for the CRX 2020 hardware build project, by **Hasse Axəlsson-Svala**
* Music.bin (inside prgPlus4 directory)
    * Demo music taken from Knaecketraecker 0.3c
* gpl.txt
    * GNU General Public License version 3

Example PRG files, taken from [CSDB](https://csdb.dk/) and [Plus/4 World](http://plus4world.powweb.com/):

* prg directory
    * example PRG files for C64
* prgPlus4 directory
    * example PRG files for Plus/4
* prg128 directory
    * example PRG files for C128
* nonworking directory
    * example PRG files for C64 that do not work when started from cartridge

## Known issues ##

* There are some C64, Plus/4 and C128 programs angd games that won't work when started from cartridge, but for now we do not know why.
A few of such C64 programs are in "nonworking" directory.
* On non-english C128, pressing the ASCII/DIN key will slightly corrupt menu graphics.
* Sometimes on C128, at the centre of the screen, two lines from the C128 monitor appear (something triggers it). Reset usually solves this.
* Having U36 on C128 populated and using Gmod2 cartridge can lead to exit to basic not working (it does not work with Servant).

If someone has some insights abot this issues and know how to fix them, plase contact us.

## Copyright ##

Magic cartridge schematics and PCB design (c) 2013-2022 Marko Šolajić

Menu for C64, C128 and Plus/4 prg and asm files and crtgen.py (c) 2013-2022 Žarko Živanov

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

**Programs in prg, prgPlus4, prg128 and nonworking directories, and also programs and games inside compilationC128-Games.bin, are copyrighted by their respective owners.**

## Sample configuration file with all descriptions ##

```
;optional section; contains cartridge information - if ommited, defaults will be used
[cartridge]

;target computer for the cartridge
;mandatory - can be either c64 or plus4
computer=c64

;cartridge size in kilobytes; can be 64, 128, 256, 512, 1024 or 0
;optional - if ommited, 0 will be used (cartridge size will be calculated on the go)
size=128

;name of generated cartridge file
;optional - if ommited, 'compilation.bin' will be used; extension can also be ommited
bin=compilation.bin

;menu file (code that executes menu in cartridge)
;optional - if ommited, 'menu-c64.prg' will be used for C64 and 'menu-plus4.prg' will be used for Plus/4; extension can also be ommited
menu=menu-c64.prg

;prg directory containing prg and sid/bin music files
;optional - if ommited, 'prg' will be used
prgdir=prg

;text for last line on screen
;optional - if ommited, default text will be used
help=(Shift)CRSR: Scroll, Fn/Ret: Menu select

;should menu wave
;optional - if ommited, menu will not wave
wave=1

;should menu have sound/music (0 - no sound)
;1 - Ross cartridge sound (popular ex-Yugoslav cartridges, VICE ID 23, C64 only)
;2 - GoatTracker SID or KnaeckeTraecker BIN music
;optional - if ommited, menu will not have sound
sound=1

;name of GoatTracker SID or KnaeckeTraecker BIN file
;optional - if ommited and sound is set to 2, menu will not have sound; extension can also be ommited (sid is used for C64 and bin for Plus/4)
music=HelloWorld

;song number to play from GoatTracker SID or KnaeckeTraecker BIN file
;optional - if ommited song 0 will be used
song=0

;colors for border, background and characters, these will be set before
;starting selected program
;optional - if ommited, defaults will be used
border=14
background=6
character=14

;same program can be at several places in one or more menus, but only one copy
;will be put into cartridge, and all its menu items will point to that copy

;optional section; can be from menu1 to menu8, contains menu information
;if ommited, program sections for menu 1 will be used (prg100, prg101, ...)
[menu1]

;menu title
;optional - if ommited, there will be no title
title=Magic Desk compatible cartridge

;order of programs in menu
;optional - if ommited, ascending order of program sections will be applied
order=05,01,02,03,04,06,07,08,09,10,11,12,13,14,15
;for menu 1, 1 is appended to prg number, for menu 2, 2 is appended, and so on
;the line above will select sections prg105, prg101, prg102 and so on

;menu width
;optional - if ommited, optimal value will be used
width=25

;menu height
;optional - if ommited, optimal value will be used
height=7

;menu x coordinate (must be 1 or higher)
;optional - if ommited, menu will be centered horizontally
x=7

;menu y coordinate (must be 1 or higher)
;optional - if ommited, menu will be centered vertically
y=3

;should menu items have an empty line between them
;optional - if ommited, there will be no epmpty lines
spacing=1

;when manually setting menu position and size, **make sure that menu will fit on screen**!

;program section, name should be 'prgMNN', where M is menu number and NN is 00-99
;if no program sections are found, prg directory will be scaned
[prg101]
;filename in prg directory; can be with or without prg extension
file=cruncher.prg
;name of program in menu; optional - if ommited, filename will be used
name=Cruncher AB 1.2
;run address; optional - if ommited or set to 0, the loading address
;from prg file will be used; can be decimal or hexadecimal format
;if program starts at 2049 ($0801), it will be started with Basic's RUN command
run=$0801

;you don't have to use successive numbers for program sections
[prg105]
file=pro_text
```

## Configuration examples ##

* Just cartridge section, no help text shown, turn on Ross sound, turn off waving
```
[cartridge]
computer=c64
size=256
help=
sound=1
wave=0
```

* Just menu section with menu title and x,y position.
   Menu will be filed with all programs in prg directory
```
[menu1]
computer=c64
title=Disk tools
x=3
y=3
```

* More detailed definition
```
[cartridge]
computer=c64
sound=2
sid=HelloWorld
song=0
wave=1

[menu1]
title=Arcade games

[menu2]
title=Logic games
order=25,20
y=2

[prg100]
file=commando
name=Commando

[prg105]
file=pacman
name=Pac-Man

[prg220]
file=tetris.prg
name=Tetris

[prg225]
file=sweep.prg
name=Mine sweep
```

## Changes in v3.8 ##

* crtgen.py
    * New option -832/-g832 for C128 cartridge with 32k banks (by Maciej 'YTM/Elysium' Witkowiak)
    * Fixed a bug when prg directory does not exist
    * Added info for Plus/4 and C128 cartconv options to convert to crt format (new in VICE 3.8)
* menu-c128.prg
    * Added support for 32k banks (by Maciej 'YTM/Elysium' Witkowiak)
    * Faster data copying with 2MHz mode and quick bank switching (by Maciej 'YTM/Elysium' Witkowiak)

## Changes in v3.7 ##

* menu-c64.prg
    * Cleanup of initial Gmod3 integration

* menu-c128.prg
    * New menu for Commodore 128, using VIC-II as output, for usage with Gmod2 for C128 (https://www.freepascal.org/~daniel/gmod2/) and Magic 128 Cartridge (not yet published)
    * Many thanks to **Daniël Mantione** for C128 Gmod2 cartridge and support
    * Be aware that programming of Gmod2 requires that U36 on C128 motherboard is empty

* crtgen.py
    * Cleanup of initial Gmod3 integration
    * Added support for Commodore 128 Gmod2/Magic 128 cartridges
    * New options -3/-g3 for Gmod3, and -8/-g8 for C128

* Makefile
    * Added Makefile for generating all prg files

## Changes in v3.6 (internal version) ##

* menu-c64.prg
    * Added initial support for Commodore 64 Gmod3 cartridges

* crtgen.py
    * Added initial support for Commodore 64 Gmod3 cartridges

## Changes in v3.5 ##

* menu-c64.prg
    * menu.prg is renamed to menu-c64.prg

* menu-plus4.prg
    * New menu for Commodore 264 range of computers, has similar capabilities as C64 version and shares good amount of code with it
    * Added support for KnaeckeTraecker music files in BIN format
    * Many thanks to people from plus4world forums, especially to **Csabo**, **Luca** and **MMS** for helping me with the code
    * Many thanks to **Gaia** for YAPE fixes and to **Groepaz** for adding Magic 264 Cartridge support to xplus4

* crtgen.py
    * Added support for Magic 264 Cartridge for Commodore 264 range of computers
    * Added configuration dictionary with differences betweeen two types of computers
    * Added options -c and -p to generate C64 or Plus/4 cartridge without configuration file
    * Added options -gc and -gp to generate initial cartridge configuration based on content of prg directory
    * Prg directory can be supplied with -p/-c/-gp/-gc options, by default **prg** is used
    * Added support for KnaeckeTraecker BIN files for Plus/4
    * Added mandatory option 'computer=' to cfg files to specify C64 or Plus/4 cartridge
    * Added alias for 'sid=' option in cfg file called 'music='
    * When configuration is used, default generated cartridge file name is same as configuration file name, unles 'bin=' option is used
    * Program renamed to Magic Cartridge Generator

## Changes in v3.1 (internal version) ##

* menu.prg
    * Fixed handling of prg files that go over I/O space at $D000
    * Added support for GoatTracker SID files, by Jim Drew, as a upgrade for CRX 2020 hardware build project
    * Fixed issue with programs that are SYS'd returning back to an uninitialized BASIC (also by Jim Drew)
    * GoatTracker SID support made configurable

* crtgen.py
    * Added support for GoatTracker SID files
    * Added option in cfg file to define directory with prg/sid files, so more compilations can be held in the same place

## Changes in v3.0 since v2.0 ##

* menu.prg
    * source transferred to Kick Assembler 5.5
    * complete rewrite of display code
    * added support for up to 8 menus, which can be selected with Fn keys or cycled through using Return
    * significantly lowered memory needed for tables and menu text, total of 256 programs can be put on cartridge (actually, I haven't tested that much, but if someone tries, let me know)
    * waving of the menu can be turned off
    * menu can produce sound used in popular ex-Yugoslav cartridges made by Ross

* crtgen.py
    * rewrite of menu tables generation
    * autosize of cartridge if size is set to zero (this is the default)
    * configurable menu position, size and item spacing
    * same program can be on several menus, only one copy will be in cartridge (but only if it is the same filename; same programs under different filenames aren't recognised)
    * configurable help text
    * prg sections in CFG file are now with 3 digits instead of 2 (adjustments must be made to configurations from v2.0)
    * more checks of configuration file

