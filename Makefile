MENU64 = menu-c64
MENU64GM3 = menu-c64-gm3
MENUPLUS4 = menu-plus4
MENU128 = menu-c128
MENU128_32K = menu-c128-32k

ifdef OS
	RM = del /Q
else
	RM = rm -f
endif

.PHONY: all clean

all: $(MENU64GM3).prg $(MENU64).prg $(MENUPLUS4).prg $(MENU128).prg $(MENU128_32K).prg
	echo Compiling ASM files

$(MENU64).prg: $(MENU64).asm
	java -jar KickAss.jar $<

$(MENU64GM3).prg: $(MENU64).asm
	java -jar KickAss.jar -define GMOD3 -o $@ $<
	mv $(MENU64).sym $(MENU64GM3).sym

$(MENUPLUS4).prg: $(MENUPLUS4).asm
	java -jar KickAss.jar $<

$(MENU128).prg: $(MENU128).asm
	java -jar KickAss.jar $<

$(MENU128_32K).prg: $(MENU128).asm
	java -jar KickAss.jar -define BANK_32K -o $@ $<
	mv $(MENU128).sym $(MENU128_32K).sym

clean: 
	$(RM) $(MENU64GM3).prg $(MENU64).prg $(MENUPLUS4).prg $(MENU128).prg $(MENU128_32K).prg

