//   Magic Desk Compatible Cartridge Generator (c) 2013-2024 Žarko Živanov
//   Cartridge schematics and PCB design (c) 2013-2021 Marko Šolajić
//   E-mails: zzarko and msolajic at gmail

//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.

//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.

//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

//   VERSION: 3.8

//BasicUpstart2(init_menu)

//--------------------------------
// RAM and ROM
//--------------------------------

.label SCREEN   = $0C00
.label COLORR   = $0800

// menu chars corners
.const MENU_CHR_UL   = $EC
.const MENU_CHR_UR   = $FB
.const MENU_CHR_DL   = $FC
.const MENU_CHR_DR   = $FE
.const MENU_COL_UL   = $5A
.const MENU_COL_UR   = $5A
.const MENU_COL_DL   = $5A
.const MENU_COL_DR   = $5A

// menu chars sides
.const MENU_CHR_L       = $61
.const MENU_CHR_R       = $E1
.const MENU_CHR_U       = $E2
.const MENU_CHR_D       = $62
.const MENU_COL_L       = $5A
.const MENU_COL_R       = $5A
.const MENU_COL_U       = $5A
.const MENU_COL_D       = $5A

// menu chars misc
.const MENU_CHR_BACK    = $20
.const MENU_CHR_SHAD    = $E6
.const MENU_COL_BACK    = $35
.const MENU_COL_SHAD    = $32
.const MENU_COL1_KEY    = $4C
.const MENU_COL2_KEY    = $5C
.const MENU_COL_NAME    = $51
.const SCREEN_CHR       = $A0
.const SCREEN_COL       = $56

.label BORDER           = $FF19
.label PAPER            = $FF15
.label IRQ_Vector       = $0314
.label IRQ_Kernel       = $CE0E
.label Kernel_IRQ_Exit  = $FCC3
.label CursorColor      = $053B

.label drive    = $AE
.label status   = $90
.label LISTEN   = $FFB1
.label SECOND   = $FF93
.label UNLSTN   = $FFAE
.label Banking  = $FDFE

//--------------------------------
// menu variables
//--------------------------------

// current menu
.label current_menu     = $61   // 1B
.label prev_menu        = $62   // 1B
.label cmenu_offset     = $63   // 2B offset from screen start
.label cmenu_offset_it  = $65   // 2B offset from screen start for items
.label cmenu_offset_key = $67   // 2B offset from screen start for item key
.label cmenu_width      = $69   // 1B menu inside width
.label cmenu_height     = $6A   // 1B menu inside height
.label cmenu_iwidth     = $6B   // 1B menu inside width minus chars for key
.label cmenu_items_no   = $6C   // 1B number of items
.label cmenu_max_first  = $6D   // 1B max value for first item
.label cmenu_items      = $6E   // 2B pointer to list of items
.label cmenu_first_idx  = $03   // 1B index of first item to show
.label cmenu_item_adr   = $9F   // 2B pointer to current item text
.label cmenu_item_idx   = $A1   // 1B current item index
.label cmenu_spacing    = $A2   // 1B menu items spacing (0-no spacing,1-1 line)

// additional menu
.label temp             = $04   // 1B temorary value
.label chrmem           = $D0   // 2B pointer to addres on screen
.label colmem           = $D2   // 2B pointer to addres in color ram
.label chrkeymem        = $D4   // 2B pointer to addres on screen for key
.label drawm_line       = $D6   // 1B current line when drawing items
.label drawm_idx        = $D7   // 1B current item index when drawing items

// key scanning
.label PressedKeyIdx    = $c6   // 1B current index in key table
.label ShiftKey         = $c7   // 1B Shift flag
.label SCNTemp          = $c0   // 1B temporary

// IRQ routine
.const WaveSpeed        = 2     // waving speed
.const FirstLine        = 11    // first raster line for waving
.label FirstWave        = $05   // index in wave table
.label WavePause        = $06   // variable for waving speed
.label RunIRQ           = $14   // does the IRQ runs or not
.label DoSoundIRQ       = $15   // does the IRQ runs or not

//.const SCR_FirstLine = 58
//.const SCR_LastLine  = 242

// used during music init:
.label zpTmp1      = $D0    // 2B
.label zpTmp2      = $D2    // 2B
.label sidStart    = $1000 // current sid settings
.label sidInit     = $1000
.label sidPlay1    = $1003
.label sidPlay2    = $1006

//--------------------------------
// Cartridge init
//--------------------------------

// Init code mainly taken from
// https://github.com/cbmuser/gamecart_plus-4

*=$8000
        jmp init_cart
        jmp init_cart
.byte $01
.encoding "petscii_upper"
.text "CBM"         // magic bytes for cartridge startup
.text "8BITCHIP"    // cartridge identification
.byte $00

init_cart:
        sei
        lda $fb
        pha
        sta $fdd2
        jsr $ff84       // Initialize I/O devices
        jsr $ff87       // RAM Test
        pla
        sta $fb
        jsr $ff8a       // Restore vectors to initial values
        jsr $ff81       // Initialize screen editor
        lda #<init_menu // cartridge jump in
        sta $02fe
        lda #>init_menu
        sta $02ff

        lda #<TEDIRQ    // new IRQ routine
        sta IRQ_Vector
        lda #>TEDIRQ
        sta IRQ_Vector+1

        ldy #$00        // it must be copied to RAM, as cartridge is turned off on IRQ
!:      lda TEDIRQ0340, y
        sta $0340,y
        iny
        cpy #TEDIRQ0340Len
        bne !-

        lda #WaveSpeed  // setup IRQ variables
        sta WavePause
        lda #00
        sta FirstWave

        lda #250        // trigger raster IRQ at line 250
        sta $FF0B       // this gives more CPU time to main program

        lda #$ff        // hide cursor
        sta $ff0c
        sta $ff0d

        lda #$D5        // D1-upper charset, D5 - lower charset
        sta $FF13

        lda #00         // disable IRQ code before menu drawing
        sta RunIRQ

        ldx #$0F        // initialize colors
!:      lda $E143,x
        sta $0113,x
        dex
        bpl !-
        lda #$00
        sta $053C

        lda DoSound     // init music if needed
        sta DoSoundIRQ  // save to RAM, as cart ROM is unavailable after IRQ
        beq !+
        jsr music_init

!:      cli
        jmp init_menu


//--------------------------------
// Raster interrupt
//--------------------------------

// Interrupt handling code taken from
// https://www.forum64.de/index.php?thread/79780-bildschirmaufteilung-auf-dem-c16-116-plus4-mit-ff0b-statt-d012/
// Many thanks to cbmhardware and Luca !

TEDIRQ0340:
.pseudopc $0340 {
TEDIRQ:
        asl $FF09   // acknowledge IRQ

        lda RunIRQ  // check if the code should be run
        beq SkipIRQ

        lda #$FE    // wait for raster on first line
!:      cmp $FF1C
        beq !-      // wait until $FF1C ≠ 0
!:      cmp $FF1C
        bne !-      // wait until $FF1C ≠ 1

        // prepare value to switch off waving
        lda $FF07   // horizontal scroll = 0
        and #$F8
        ora #$08    // 40 columns
        pha

        lda #FirstLine
        ldy FirstWave

WaveLine:
        pha
        lda WaveTable, y
        tax
        pla
!:      cmp $FF1D   // wait for first raster line in character row
        bne !-
        stx $FF07
        cmp #FirstLine + 23*8
        beq TEDIRQend

        iny
        cpy #WaveTableMax
        bne !+
        ldy #00

!:      clc     // next character row
        adc #$08
        cmp #FirstLine + 23*8   // if last waving row, next value should be 0
        bne WaveLine
        ldy #00
!:      beq WaveLine

TEDIRQend:
        pla
        sta $FF07

        dec WavePause   // adjust waving
        bne !+
        lda #WaveSpeed
        sta WavePause
        dec FirstWave
        bpl !+
        lda #WaveTableMax-1
        sta FirstWave

!: SkipIRQ:
        lda DoSoundIRQ  // play music
        cmp #$02
        bne !+
        jsr sidPlay1
        jsr sidPlay2

!:      lda $fb
        sta $fdd2   // enable cartridge ROM

        jmp Kernel_IRQ_Exit

WaveTable: .byte $00,$00,$01,$01,$02,$03,$04,$05,$05,$06,$06,$07,$07
           .byte $07,$07,$06,$06,$05,$05,$04,$03,$02,$01,$01,$00,$00
.label WaveTableMax = *-WaveTable
}
.label TEDIRQ0340Len = *-TEDIRQ0340

//--------------------------------
// menu drawing
//--------------------------------

init_menu:
        lda #0
        sta current_menu
        lda #$FF
        sta prev_menu

dm0:    lda current_menu
        cmp prev_menu
        beq dm2
        sta prev_menu
        lda #00
        sta RunIRQ
        lda #$00
        ldx #SCREEN_CHR
        ldy #SCREEN_COL
        jsr clear_screen
        jsr setup_menu
        lda #0
        sta cmenu_first_idx
        jsr draw_menu_border

dm1:    lda cmenu_first_idx
        sta drawm_idx
        jsr draw_menu_items
        lda DoWave
        sta RunIRQ

dm2:    jsr read_key
        beq dm2

        cmp #$1D  // left-right key
        bne !++
        lda cmenu_first_idx
        cmp cmenu_max_first
        bpl !+
        inc cmenu_first_idx
!:      jmp dm1

!:      cmp #$11  // up-down key
        bne !++
        lda cmenu_first_idx
        beq !+
        dec cmenu_first_idx
!:      jmp dm1

!:      cmp #$9D  // shift+left-right key
        bne !++
        lda cmenu_first_idx
        clc
        adc cmenu_height
        sta cmenu_first_idx
        cmp cmenu_max_first
        bmi !+
        lda cmenu_max_first
        sta cmenu_first_idx
!:      jmp dm1

!:      cmp #$91  // shift+up-down key
        bne !++
        lda cmenu_first_idx
        sec
        sbc cmenu_height
        sta cmenu_first_idx
        cmp #0
        bpl !+
        lda #0
        sta cmenu_first_idx
!:      jmp dm1

!:      cmp #$0D  // return
        beq nextmenu
        cmp #$19  // left arrow
        beq basic
        cmp #$85  // F1
        bmi prgkey
        cmp #$8D  // 89 - F7+1, 8D - F8+1
        bpl prgkey
        sec
        sbc #$85
        tay
        lda menu_items_no,y
        beq dm2
        sty current_menu
        jmp dm0

prgkey: ldy #0  // check if some program is selected
!:      cmp menu_keys,y
        beq !+
        iny
        cpy #menu_keys_no
        bne !-
        jmp dm2
!:      iny  // last item is basic, do not check that key
        tya
        ldy current_menu
        cmp menu_items_no,y
        bmi !+
        jmp dm2
!:      tay
        dey
        tya
        jmp prepare_run

basic:
        ldy cmenu_items_no
        dey
        tya
        jmp prepare_run

nextmenu:
        ldy current_menu
        sty temp
        iny
        cpy #8
        bne !+
        ldy #0
!:      lda menu_items_no,y
        sty current_menu
        bne !+
        lda #0
        sta current_menu
!:      jmp dm0

draw_menu_border:
        lda cmenu_offset
        sta chrmem
        sta colmem
        lda #>SCREEN
        clc
        adc cmenu_offset+1
        sta chrmem+1
        lda #>COLORR
        clc
        adc cmenu_offset+1
        sta colmem+1

        ldx #0
        jsr draw_menu_border_line
        jsr menu_next_line

        lda cmenu_height
        ldy cmenu_spacing
        beq dmb1
        asl
        tay
        dey
        tya
dmb1:   pha
        ldx #4
        jsr draw_menu_border_line
        lda cmenu_spacing
        beq !+
        pla
        pha
        and #1
        beq dmb2
!:      ldy #1
        ldx #0
!:      lda menu_key_chr,x
        sta (chrmem),y
        lda menu_key_col,x
        sta (colmem),y
        iny
        inx
        cpx #3
        bmi !-
dmb2:   jsr menu_next_line
        pla
        tay
        dey
        tya
        bne dmb1

        ldx #8
        jsr draw_menu_border_line
        jsr menu_next_line
        ldx #12
        jsr draw_menu_border_line
        rts

draw_menu_border_line:
        ldy #0
        lda menu_border_chr,x
        sta (chrmem),y
        lda menu_border_col,x
        sta (colmem),y
        iny
        lda cmenu_width
        sta temp
        inc temp
        inx
!:      lda menu_border_chr,x
        sta (chrmem),y
        lda menu_border_col,x
        sta (colmem),y
        iny
        cpy temp
        bmi !-
        inx
        lda menu_border_chr,x
        sta (chrmem),y
        lda menu_border_col,x
        sta (colmem),y
        inx
        iny
        lda menu_border_chr,x
        sta (chrmem),y
        lda menu_border_col,x
        sta (colmem),y
        rts

menu_next_line:
        clc
        lda chrmem
        adc #40
        sta chrmem
        sta colmem
        bcc !+
        inc chrmem+1
        inc colmem+1
!:      clc
        lda chrkeymem
        adc #40
        sta chrkeymem
        bcc !+
        inc chrkeymem+1
!:      rts

draw_menu_items:
        lda cmenu_offset_it
        sta chrmem
        sta colmem
        lda #>SCREEN
        clc
        adc cmenu_offset_it+1
        sta chrmem+1
        lda #>COLORR
        clc
        adc cmenu_offset_it+1
        sta colmem+1

        lda cmenu_offset_key
        sta chrkeymem
        lda #>SCREEN
        clc
        adc cmenu_offset_key+1
        sta chrkeymem+1

        lda drawm_idx
        jsr find_menu_item
        lda #0
        sta drawm_line
!:      jsr draw_menu_item_line
        jsr menu_next_line
        jsr find_next_menu_item
        ldy cmenu_spacing
        beq dmi1
        jsr menu_next_line
dmi1:   inc drawm_idx
        inc drawm_line
        lda drawm_line
        cmp cmenu_height
        bne !-
        rts

draw_menu_item_line:
        ldy #0
!:      lda (cmenu_item_adr),y
        beq !+
        sta (chrmem),y
        iny
        cpy cmenu_iwidth
        bne !-
!:      lda menu_chr_back
        cpy cmenu_iwidth
        beq !+
        sta (chrmem),y
        iny
        bne !-
!:      ldy drawm_idx   // convert PETSCII to inverted SCREEN codes
        iny             // last item will always be "Basic", and its key is Escape
        cpy cmenu_items_no
        bne !+
        ldy #menu_keys_no+1
!:      dey
        lda menu_keys,y
        cmp #$C1        // codes C1-DA translate to C1-DA
        bcs dmil1
        cmp #$40
        bcc !+
        clc             // codes 41-5A translate to 81-9A
        adc #$40
        .byte $2C       // bit instruction, skip ora
!:      ora #$80        // codes 41-5A translate to B0-B9
dmil1:  ldy #0
        sta (chrkeymem),y
        rts

//--------------------------------
// item list functions
//--------------------------------

// find menu item whose index is in A register and set menu item pointer
find_menu_item:
        sta cmenu_item_idx
        lda cmenu_items
        sta cmenu_item_adr
        lda cmenu_items+1
        sta cmenu_item_adr+1
        ldx #0
fmi1:   cpx cmenu_item_idx
        beq fmi2
        ldy #0
!:      lda (cmenu_item_adr),y
        beq !+
        iny
        bne !-
!:      iny
        tya
        clc
        adc cmenu_item_adr
        bcc !+
        inc cmenu_item_adr+1
!:      sta cmenu_item_adr
        inx
        jmp fmi1
fmi2:   rts

find_next_menu_item:
        ldx cmenu_item_idx
        inc cmenu_item_idx
        jmp fmi1

//--------------------------------
// menu setup
//--------------------------------

// setup all current menu variables for menu whose index is in current_menu
setup_menu:
        ldx current_menu
        ldy menu_items_no,x
        sty cmenu_items_no
        tya
        sec
        sbc menu_height,x
        sta cmenu_max_first
        ldy menu_width,x
        sty cmenu_width
        dey
        dey
        dey
        sty cmenu_iwidth        // menu_width-3 (width of key display)
        ldy menu_height,x
        sty cmenu_height
        ldy menu_spacing,x
        sty cmenu_spacing
        txa                     // offsets are 2-byte
        asl
        tax
        lda menu_offset,x
        sta cmenu_offset
        lda menu_offset+1,x
        sta cmenu_offset+1
        lda menu_items,x
        sta cmenu_items
        lda menu_items+1,x
        sta cmenu_items+1
        // calculate offset for menu items
        lda cmenu_offset+1
        sta cmenu_offset_it+1
        lda cmenu_offset
        clc
        adc #44                 // 1 line + 3 chars for key display
        sta cmenu_offset_it
        bcc !+
        inc cmenu_offset_it+1
        // calculate offset for menu item keys
!:      lda cmenu_offset+1
        sta cmenu_offset_key+1
        lda cmenu_offset
        clc
        adc #42                 // 1 line + 2
        sta cmenu_offset_key
        bcc !+
        inc cmenu_offset_key+1

!:      lda menu_names,x
        sta chrmem
        lda menu_names+1,x
        sta chrmem+1
        ldy #0
!:      lda (chrmem),y
        ora #$80
        sta SCREEN,y
        lda menu_help,y
        ora #$80
        sta SCREEN+24*40,y
        iny
        cpy #40
        bne !-
        rts

//--------------------------------
// utility functions
//--------------------------------

// A - border/paper, X - char, Y - color
clear_screen:
        sta PAPER
        sty BORDER
        txa
        ldx #0
!:      sta SCREEN,x
        sta SCREEN+$100,x
        sta SCREEN+$200,x
        sta SCREEN+$2E8,x
        pha
        tya
        sta COLORR,x
        sta COLORR+$100,x
        sta COLORR+$200,x
        sta COLORR+$2E8,x
        pla
        inx
        bne !-
        rts

// returns key as read by SCNKEY, makes a small pause before reading
read_key:
        ldy #40
        ldx #0
!:      dex
        bne !-
        dey
        bne !-
!:      jsr SCNKEY
        beq !-
        rts

// find first active drive
FindFirstDrive:
        lda #8
        sta drive
!:      ldx #0
        stx status
        jsr LISTEN
        lda #$FF
        jsr SECOND
        lda status
        bpl !+
        jsr UNLSTN
        inc drive
        lda drive
        cmp #31
        bne !-
        lda #8          // if not found, set 08
        sta drive
!:      jsr UNLSTN
        rts


//--------------------------------
//      Keyboard scanning
//--------------------------------

SCNKEY:
        lda #$FE
        sta PressedKeyIdx
        inc PressedKeyIdx   // $FF - no key pressed
        ldy #$00            // main key table index
        sty ShiftKey        // no Shift pressed

SCN_Row:
        pha
        sta $FD30           // prepare keyboard latch
        ldx #$FF
        stx $FF08           // do not read joystick
        lda $FF08           // read key row
        cmp #$FF
        beq SCN_NextRow     // nothing pressed

SCN_FindKey:                // find pressed key (checks keys all in current column, last one pressed is saved)
        ldx #$00            // current row key table index
SCN_CheckBit:
        lsr
        bcc !+              // check if bit is zero
        inx                 // if not, increase row index
        cpx #$08
        bne SCN_CheckBit
        beq SCN_NextRow
!:      stx SCNTemp         // add current row index
        pha
        tya                 // to main key table index
        clc
        adc SCNTemp
        tax                 // read key from table
        lda KeyTable, x
        cmp #$16            // if Shift
        bne !+
        inc ShiftKey        // set Shift flag
        bne !++
!:      stx PressedKeyIdx
!:      pla
        ldx SCNTemp
        inx
        cpx #$08
        bne SCN_CheckBit

SCN_NextRow:
        tya                 // prepare next row main index
        clc
        adc #$08
        tay
        pla                 // prepare next bit mask for keyboard latch
        sec
        rol
        bcs SCN_Row         // read next row, if Carry = 0, then finish

SCN_End:
        ldx PressedKeyIdx   // read key from the table
        lda #$16            // assume Shift is pressed
        cpx #$FF            // check if there was a keypress
        bne !++             // if yes, determine which one
        ldx ShiftKey        // if not, check if Shift was pressed
        bne !+
        lda #00             // if not, return zero
!:      rts
!:      lda ShiftKey        // check which table to read
        bne !+
        lda KeyTable, x
        rts
!:      lda KeyTableShift, x
        rts

/*
http://plus4world.powweb.com/plus4encyclopedia/500012
https://sta.c64.org/cbm64pet.html

Keyboard matrix

          $FE       $FD       $FB       $F7       $EF       $DF       $BF       $7F
$FE($01)  ins/del   Return    Pound     Help/F7   F1/F4     F2/F5     F3/F6     @
$FD($02)  3         w         a         4         z         s         e         Shift
$FB($04)  5         r         d         6         c         f         t         x
$F7($08)  7         y         g         8         b         h         u         v
$EF($10)  9         i         j         0         m         k         o         n
$DF($20)  Down      p         l         Up        >         [         -         <
$BF($40)  Left      *         ]         Right     Escape    =         +         /
$7F($80)  1         Clr/Home  Control   2         Space     Commodore q         Run/Stop

As those aren't defined in PETSCII, I used these constants for control keys:

Shift - $16
Ctrl  - $17
Comm  - $18
Esc   - $19
Run/S - $1A

In the table below, function key codes were changed to be like on C64, as the structure
of menus is organized on that assumption. Also, cursor key codes are adjusted for C64
version of menu code, to minimze the effort of maintaining both versions of the code.

*/

KeyTable:
    .byte $14, $0D, $5C, $88, $85, $86, $87, $40    // $FE
    .byte $33, $57, $41, $34, $5A, $53, $45, $16    // $FD
    .byte $35, $52, $44, $36, $43, $46, $54, $58    // $FB
    .byte $37, $59, $47, $38, $42, $48, $55, $56    // $F7
    .byte $39, $49, $4A, $30, $4D, $4B, $4F, $4E    // $EF
    .byte $1D, $50, $4C, $11, $3E, $5B, $2D, $3C    // $DF
    .byte $91, $2A, $5D, $9D, $19, $3D, $2B, $2F    // $BF
    .byte $31, $13, $17, $32, $20, $18, $51, $1A    // $7F

/*
// this table has normal cursor key codes for Plus4
KeyTable:
    .byte $14, $0D, $5C, $88, $85, $86, $87, $40    // $FE
    .byte $33, $57, $41, $34, $5A, $53, $45, $16    // $FD
    .byte $35, $52, $44, $36, $43, $46, $54, $58    // $FB
    .byte $37, $59, $47, $38, $42, $48, $55, $56    // $F7
    .byte $39, $49, $4A, $30, $4D, $4B, $4F, $4E    // $EF
    .byte $11, $50, $4C, $91, $3E, $5B, $2D, $3C    // $DF
    .byte $9D, $2A, $5D, $1D, $19, $3D, $2B, $2F    // $BF
    .byte $31, $13, $17, $32, $20, $18, $51, $1A    // $7F
*/

KeyTableShift:
    .byte $94, $0D, $5C, $8C, $89, $8A, $8B, $40    // $FE
    .byte $33, $D7, $C1, $34, $DA, $D3, $C5, $16    // $FD
    .byte $35, $D2, $C4, $36, $C3, $C6, $D4, $D8    // $FB
    .byte $37, $D9, $C7, $38, $C2, $C8, $D5, $D6    // $F7
    .byte $39, $C9, $CA, $30, $CD, $CB, $CF, $CE    // $EF
    .byte $11, $D0, $CC, $91, $3E, $5B, $2D, $3C    // $DF
    .byte $9D, $2A, $5D, $1D, $19, $3D, $2B, $2F    // $BF
    .byte $31, $93, $17, $32, $20, $18, $D1, $1A    // $7F

//--------------------------------
// Memory copy
//--------------------------------

.label TableAddress  = $61
.label ProgramIndex  = $63

prepare_run:

        sta ProgramIndex
        ldy #0
!:      cpy current_menu
        beq !+
        ldx menu_items_no,y
        txa
        clc
        adc ProgramIndex
        sta ProgramIndex
        iny
        bne !-
!:      jsr FindFirstDrive
        sei

startCopy:
        ldy #CartCopyLen    // copy routine to 0340
!:      lda CartCopy0340,y
        sta $0340,y
        dey
        cpy #$ff
        bne !-
        ldy #0              //calculate program table element address
        sty TableAddress+1  //each table element is 9 bytes
        lda ProgramIndex    //ProgramIndex*8
        asl
        rol TableAddress+1
        asl
        rol TableAddress+1
        asl
        rol TableAddress+1
        sta TableAddress
        lda ProgramIndex    //ProgramIndex*8 + ProgramIndex
        clc
        adc TableAddress
        sta TableAddress
        bcc sc2
        inc TableAddress+1
sc2:    lda ProgramTable    //add Program table address
        clc
        adc TableAddress
        sta TableAddress
        lda ProgramTable+1
        adc TableAddress+1
        sta TableAddress+1
        lda (TableAddress),y    //set values in copy routine
        sta CartBank
        iny;lda (TableAddress),y
        sta CartAddr
        iny;lda (TableAddress),y
        sta CartAddr+1
        iny;lda (TableAddress),y
        sta PrgLenLo
        iny;lda (TableAddress),y
        sta PrgLenHi
        iny;lda (TableAddress),y
        sta MemAddr
        iny;lda (TableAddress),y
        sta MemAddr+1
        iny;lda (TableAddress),y
        sta pstart+1
        iny;lda (TableAddress),y
        sta pstart+2
        beq sc3     //if high byte of start address != 0
        cmp #$8A
        bne !+
        lda #$20
        sta basmsg
!:      lda #$20    //change lda to jsr
        sta pstart  //else, run as basic
sc3:    jmp $0340

CartCopy0340:
.pseudopc $0340 {
        sei
        ldx PrgLenLo:#00    //program length lo
        ldy PrgLenHi:#00    //program length hi
cbank:
        lda CartBank:#00    //cartridge start bank
        sta Banking         //cartridge bank switching address
caddr:  lda CartAddr:$8200  //cartridge start adr
        sta MemAddr:$0801   //c64 memory start adr
        dex
        cpx #$ff
        bne cc1
        dey
        cpy #$ff
        beq crtoff
cc1:    inc MemAddr
        bne cc2
        inc MemAddr+1
cc2:    inc CartAddr
        bne caddr
        inc CartAddr+1
        lda CartAddr+1
        cmp #$c0        //next bank?
        bne caddr
        inc CartBank
        lda #$80        //cartridge bank is on $8000-$BFFF
        sta CartAddr+1
        jmp cbank
crtoff:
        lda #$ff        //go to last bank
        sta Banking
        lda MemAddr     //set end of program (var start)
        sta $2d
        sta $2f
        sta $31
        lda MemAddr+1
        sta $2e
        sta $30
        sta $32

        // Program run code mainly taken from
        // https://github.com/cbmuser/gamecart_plus-4
        lda #$00
        sta $02fe
        sta $02ff
        lda #$a4         //  native irq
        sta IRQ_Vector
        lda #$f2
        sta IRQ_Vector+1
        lda #$00         // basic , kernal
        sta $fdd0        // function rom off
        sta $fb
        lda $fb  
        jsr $ff84        // Initialize I/O devices 
        jsr $ff8a        // Restore vectors to initial values
        jsr $8117        // restore vectors !
        jsr $ff81        // Initialize screen editor
        jsr $802E
basmsg: lda $80C2
        cli

        jsr $802e        //; init Basic RAM
        jsr $8818        //; prg link
        jsr $8bbe        //; prg mode

        lda #$ff
        sta $ff0c
        sta $ff0d           // hide cursor

//        lda RunBorderColor
//        sta BORDER
//        lda RunPaperColor
//        sta PAPER
//        lda RunInkColor
//        sta CursorColor

pstart: lda $1001
        jsr $8bea        //; start prg
        rts
}
.label CartCopyLen = *-CartCopy0340

//--------------------------------
// Music init
//--------------------------------

music_init:
move1:  lda sidStartAdr
        sta zpTmp1           // zpTmp1 is source pointer
        lda sidStartAdr+1
        sta zpTmp1+1
        lda #<sidStart
        sta zpTmp2           // zpTmp2 is dest pointer
        lda #>sidStart
        sta zpTmp2+1

        ldy #$00
        ldx #$00             // x = count of pages moved (1K = 4 pages)
move2:  lda (zpTmp1),y
        sta (zpTmp2),y
        iny
        bne move2
        inc zpTmp1+1
        inc zpTmp2+1
        inx
        cpx sidPages         // stop after all pages moved
        bne move2

        lda sidSubtune       // select subtune
        jsr sidInit          // init music
        rts

//--------------------------------
// menu data
//--------------------------------

.encoding "screencode_mixed"

// menu border first, middle, bottom and last line chars
menu_border_chr:
        .byte MENU_CHR_UL,MENU_CHR_U,MENU_CHR_UR,SCREEN_CHR
        .byte MENU_CHR_L,MENU_CHR_BACK,MENU_CHR_R,MENU_CHR_SHAD
        .byte MENU_CHR_DL,MENU_CHR_D,MENU_CHR_DR,MENU_CHR_SHAD
        .byte SCREEN_CHR,MENU_CHR_SHAD,MENU_CHR_SHAD,MENU_CHR_SHAD

// menu border first, middle, bottom and last line colors
menu_border_col:
        .byte MENU_COL_UL,MENU_COL_U,MENU_COL_UR,SCREEN_COL
        .byte MENU_COL_L,MENU_COL_BACK,MENU_COL_R,MENU_COL_SHAD
        .byte MENU_COL_DL,MENU_COL_D,MENU_COL_DR,MENU_COL_SHAD
        .byte SCREEN_COL,MENU_COL_SHAD,MENU_COL_SHAD,MENU_COL_SHAD

// misc menu chars
menu_chr_back:  .byte MENU_CHR_BACK
menu_key_chr:   .byte $F5,$B1,$F6
menu_key_col:   .byte MENU_COL1_KEY,MENU_COL2_KEY,MENU_COL1_KEY

// keys for selecting a program
menu_keys:      .fill 9,i+$31   // 1-9
                .byte $30       // 0
                .fill 26,i+$41  // a-z
                .fill 26,i+$C1  // A-Z
.label menu_keys_no = *-menu_keys
                .byte $5F       // left arrow

*=* "Menu Data" virtual

sidStartAdr:    .word $0000
sidPages:       .byte $06   // number of pages (256 bytes) this SID takes up
sidSubtune:     .byte $00   // SIDs can possibly contain multiple songs

ProgramTable:   .word %0000

// program run colors
RunBorderColor: .byte 12 //14
RunPaperColor:  .byte 15 //6
RunInkColor:    .byte 0  //14
DoWave:         .byte 1  //menu waving
DoSound:        .byte 1  //menu sound

// menu data for 8 menus
menu_items_no:  .byte 1,0,0,0,0,0,0,0
menu_offset:    .word 85,0,0,0,0,0,0,0
menu_width:     .byte 15,0,0,0,0,0,0,0
menu_height:    .byte 10,0,0,0,0,0,0,0
menu_spacing:   .byte 1,0,0,0,0,0,0,0

menu_names:     .word menu_name1,0,0,0,0,0,0,0
menu_items:     .word menu_items1,0,0,0,0,0,0,0

menu_help:      .text "Cursor: Scroll, Fn/Help/Ret: Menu select"

menu_name1:     .text "            Testing...                  "

menu_items1:
        .text @"Testing1\$00"
        .text @"Testing2\$00"

programs:
// program table
// .byte bank
// .word address in bank
// .word length
// .word load address
// .word start address
//     if hi byte=0, then run as basic

