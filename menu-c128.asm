//   Magic Desk Compatible Cartridge Generator (c) 2013-2024 Žarko Živanov
//   32k banks/2MHz code by Maciej 'YTM/Elysium' Witkowiak
//   Cartridge schematics and PCB design (c) 2013-2021 Marko Šolajić
//   E-mails: zzarko and msolajic at gmail

//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.

//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.

//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

//   VERSION: 3.8

// define this to use 32k banks instead of 16k
//#define BANK_32K

/*
VIC         D000
SID         D400
MMU         D500
VDC         D600
Color RAM   D800
CIA 1       DC00
CIA 2       DD00

2D 2E - TXTTAB Start-of-BASIC-program pointer
2F 30 - VARTAB Start-of-variables pointer
31 32 - ARYTAB 49-50 Start-of-arrays pointer
33 34 - STREND Start-of-free-memory pointer
35 36 - FRETOP Bottom-of-string-space pointer
39 3A - MAX_MEM_1 Top-of-memory pointer
*/

// C128 ZP f-flag, t-temporary stroage, b-temporary buffer(these first!), m-monitor vars
//                                        ** ** ** ** ** ** ** ** **             ** ** ** ** **
//  T  T  f  T  f  f  f  T  f  f  f        b  b  b  b  b  b  b  b  b  T  T  T  T  b  b  b  b  b
// 0A 0B 0C 0D 0E 0F 10 11 12 13 14 16 17 1B 1C 1D 1E 1F 20 21 22 23 24 25 26 27 28 29 2A 2B 2C
//                            ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **
//  t  t  t  t  t  t  t  t  f  b  b  b  b  b  b  b  b  b  b  b  b  b  b  b  b  b  b  b  b  b  f
// 37 38 41 42 43 44 4D 4E 4F 50 51 52 53 54 59 5A 5B 5C 5D 5E 5F 60 61 62 63 64 65 66 67 68 69
//
//  b  b  b  b  b  b  T  T  T  T  T  T  T
// 6A 6B 6C 6D 6E 6F 70 71 72 73 77 78 79

//--------------------------------
// RAM and ROM
//--------------------------------

.label SCREEN   = $0400
.label COLORR   = $D800
.label CHROUT   = $FFD2
.label GETIN    = $FFE4
.label SCNKEY   = $FF9F
.label COLDRES  = $FCE2
.label BORDER   = $D020
.label PAPER    = $D021
.label IRQ_Vector = $0314
.label IRQ_Kernel = $FA65
.label CursorColor = $F1
.label CartCopyAddress = $0800
.label DriveFound = $77

.label drive    = $BA
.label status   = $90
.label LISTEN   = $FFB1
.label SECOND   = $FF93
.label UNLSTN   = $FFAE

// used during music init:
.label zpTmp1      = $FB // 2B
.label zpTmp2      = $FD // 2B

.label sidStart    = $1000 // current sid settings
.label sidInit     = $1000
.label sidPlay     = $1003

.label DoSoundRAM  = $67   // copy in RAM because IRQ runs with cartridge off
.label DoWaveRAM   = $68   // copy in RAM because IRQ runs with cartridge off
//--------------------------------
// menu variables
//--------------------------------

// current menu variables
.label current_menu     = $23   // 1B
.label prev_menu        = $2C   // 1B
.label cmenu_offset     = $1B   // 2B offset from screen start
.label cmenu_offset_it  = $1D   // 2B offset from screen start for items
.label cmenu_offset_key = $1F   // 2B offset from screen start for item key
.label cmenu_width      = $52   // 1B menu inside width
.label cmenu_height     = $53   // 1B menu inside height
.label cmenu_iwidth     = $54   // 1B menu inside width minus chars for key
.label cmenu_items_no   = $59   // 1B number of items
.label cmenu_max_first  = $5A   // 1B max value for first item
.label cmenu_items      = $5B   // 2B pointer to list of items
.label cmenu_first_idx  = $66   // 1B index of first item to show
.label cmenu_item_adr   = $21   // 2B pointer to current item text
.label cmenu_item_idx   = $5D   // 1B current item index
.label cmenu_spacing    = $5E   // 1B menu items spacing (0-no spacing,1-1 line)

// misc menu variables
.label temp             = $5F   // 1B temorary value
.label chrmem           = $28   // 2B pointer to addres on screen
.label colmem           = $2A   // 2B pointer to addres in color ram
.label chrkeymem        = $50   // 2B pointer to addres on screen for key
.label drawm_line       = $60   // 1B current line when drawing items
.label drawm_idx        = $61   // 1B current item index when drawing items

//--------------------------------
// IRQ variables
//--------------------------------

.const SCR_FirstLine = 58
.const SCR_LastLine  = 242

// IRQ wave variables
.const WaveSpeed     = 2
.label CurrentLine   = $62
.label CurrentWave   = $63
.label FirstWave     = $64
.label WavePause     = $65
.label C128IRQRAMLoc = $0800

//--------------------------------
// cartridge start
//--------------------------------

* = $8000
cart_vectors:
jmp cold_start
jmp cold_start
.byte $02,$43,$42,$4d // cartridge magic bytes
cold_start:
        sei
        lda #$0A        // MMU - turn on KERNAL
        sta $FF00
        lda #$FF        // turn off screen editor
        sta $00D8

        // save 1000-1300 to 3000 (music goes to 1000)
        lda #00
        sta zpTmp1
        sta zpTmp2
        lda #$10
        sta zpTmp1+1
        lda #$30
        sta zpTmp2+1
        ldx #$03
        ldy #00
!:      lda (zpTmp1),y
        sta (zpTmp2),y
        iny
        bne !-
        inc zpTmp1+1
        inc zpTmp2+1
        dex
        bne !-
        cli

//--------------------------------
// menu drawing
//--------------------------------

init_menu:
        lda DoWave
        sta DoWaveRAM
        lda DoSound
        sta DoSoundRAM
        cmp #$00
        beq !++
        cmp #$02
        beq !+
        jsr sound_init
        jmp !+
!:      jsr music_init

!:      jsr WaveVIC
        lda #$17    // lower chars
        sta $D018

        lda #0
        sta current_menu
        lda #$FF
        sta prev_menu

dm0:    lda current_menu
        cmp prev_menu
        beq dm2
        sta prev_menu
        lda #6
        sta BORDER
        lda #0
        sta PAPER
        ldx #$A0
        ldy #6
        jsr clear_screen
        jsr setup_menu
        lda #0
        sta cmenu_first_idx
        jsr draw_menu_border

dm1:    lda cmenu_first_idx
        sta drawm_idx
        jsr draw_menu_items

//        lda $0ac5
//        ora #$80
//        sta $0ac5

dm2:
//        lda $00
////        and #$BF
//        ora #$40
//        sta $00
//        lda $01
//        and #$BF
//        sta $01
        jsr read_key
        beq dm2

        cmp #$1D  // left-right key
        bne !++
        lda cmenu_first_idx
        cmp cmenu_max_first
        bpl !+
        inc cmenu_first_idx
!:      jmp dm1

!:      cmp #$11  // up-down key
        bne !++
        lda cmenu_first_idx
        beq !+
        dec cmenu_first_idx
!:      jmp dm1

!:      cmp #$9D  // shift+left-right key
        bne !++
        lda cmenu_first_idx
        clc
        adc cmenu_height
        sta cmenu_first_idx
        cmp cmenu_max_first
        bmi !+
        lda cmenu_max_first
        sta cmenu_first_idx
!:      jmp dm1

!:      cmp #$91  // shift+up-down key
        bne !++
        lda cmenu_first_idx
        sec
        sbc cmenu_height
        sta cmenu_first_idx
        cmp #0
        bpl !+
        lda #0
        sta cmenu_first_idx
!:      jmp dm1

!:      cmp #$0D  // return
        beq nextmenu
        cmp #$5F  // left arrow
        beq basic
        cmp #$85  // F1
        bmi prgkey
        cmp #$8D  // 89 - F7+1, 8D - F8+1
        bpl prgkey
        sec
        sbc #$85
        tay
        lda menu_items_no,y
        beq dm2
        sty current_menu
        jmp dm0

prgkey: ldy #0  // check if some program is selected
!:      cmp menu_keys,y
        beq !+
        iny
        cpy #menu_keys_no
        bne !-
        jmp dm2
!:      iny  // last item is basic, do not check that key
        tya
        ldy current_menu
        cmp menu_items_no,y
        bmi !+
        jmp dm2
!:      tay
        dey
        tya
        jmp prepare_run

basic:
        ldy cmenu_items_no
        dey
        tya
        jmp prepare_run

nextmenu:
        ldy current_menu
        sty temp
        iny
        cpy #8
        bne !+
        ldy #0
!:      lda menu_items_no,y
        sty current_menu
        bne !+
        lda #0
        sta current_menu
!:      jmp dm0

draw_menu_border:
        lda cmenu_offset
        sta chrmem
        sta colmem
        lda #>SCREEN
        clc
        adc cmenu_offset+1
        sta chrmem+1
        lda #>COLORR
        clc
        adc cmenu_offset+1
        sta colmem+1

        ldx #0
        jsr draw_menu_border_line
        jsr menu_next_line

        lda cmenu_height
        ldy cmenu_spacing
        beq dmb1
        asl
        tay
        dey
        tya
dmb1:   pha
        ldx #4
        jsr draw_menu_border_line
        lda cmenu_spacing
        beq !+
        pla
        pha
        and #1
        beq dmb2
!:      ldy #1
        ldx #0
!:      lda menu_key_chr,x
        sta (chrmem),y
        lda menu_key_col,x
        sta (colmem),y
        iny
        inx
        cpx #3
        bmi !-
dmb2:   jsr menu_next_line
        pla
        tay
        dey
        tya
        bne dmb1

        ldx #8
        jsr draw_menu_border_line
        jsr menu_next_line
        ldx #12
        jsr draw_menu_border_line
        rts

draw_menu_border_line:
        ldy #0
        lda menu_border_chr,x
        sta (chrmem),y
        lda menu_border_col,x
        sta (colmem),y
        iny
        lda cmenu_width
        sta temp
        inc temp
        inx
!:      lda menu_border_chr,x
        sta (chrmem),y
        lda menu_border_col,x
        sta (colmem),y
        iny
        cpy temp
        bmi !-
        inx
        lda menu_border_chr,x
        sta (chrmem),y
        lda menu_border_col,x
        sta (colmem),y
        inx
        iny
        lda menu_border_chr,x
        sta (chrmem),y
        lda menu_border_col,x
        sta (colmem),y
        rts

menu_next_line:
        clc
        lda chrmem
        adc #40
        sta chrmem
        sta colmem
        bcc !+
        inc chrmem+1
        inc colmem+1
!:      clc
        lda chrkeymem
        adc #40
        sta chrkeymem
        bcc !+
        inc chrkeymem+1
!:      rts

draw_menu_items:
        lda cmenu_offset_it
        sta chrmem
        sta colmem
        lda #>SCREEN
        clc
        adc cmenu_offset_it+1
        sta chrmem+1
        lda #>COLORR
        clc
        adc cmenu_offset_it+1
        sta colmem+1

        lda cmenu_offset_key
        sta chrkeymem
        lda #>SCREEN
        clc
        adc cmenu_offset_key+1
        sta chrkeymem+1

        lda drawm_idx
        jsr find_menu_item
        lda #0
        sta drawm_line
!:      jsr draw_menu_item_line
        jsr menu_next_line
        jsr find_next_menu_item
        ldy cmenu_spacing
        beq dmi1
        jsr menu_next_line
dmi1:   inc drawm_idx
        inc drawm_line
        lda drawm_line
        cmp cmenu_height
        bne !-
        rts

draw_menu_item_line:
        ldy #0
!:      lda (cmenu_item_adr),y
        beq !+
        sta (chrmem),y
        iny
        cpy cmenu_iwidth
        bne !-
!:      lda menu_chr_back
        cpy cmenu_iwidth
        beq !+
        sta (chrmem),y
        iny
        bne !-
!:      ldy drawm_idx   // convert PETSCII to inverted SCREEN codes
        iny             // last item will always be "Basic", and its key is left arrow
        cpy cmenu_items_no
        bne !+
        ldy #menu_keys_no+1
!:      dey
        lda menu_keys,y
        cmp #$C1        // codes C1-DA translate to C1-DA
        bcs dmil1
        cmp #$40
        bcc !+
        clc             // codes 41-5A translate to 81-9A
        adc #$40
        .byte $2C       // bit instruction
!:      ora #$80        // codes 41-5A translate to B0-B9
dmil1:  ldy #0
        sta (chrkeymem),y
        rts

//--------------------------------
// item list functions
//--------------------------------

// find menu item whose index is in A register and set menu item pointer
find_menu_item:
        sta cmenu_item_idx
        lda cmenu_items
        sta cmenu_item_adr
        lda cmenu_items+1
        sta cmenu_item_adr+1
        ldx #0
fmi1:   cpx cmenu_item_idx
        beq fmi2
        ldy #0
!:      lda (cmenu_item_adr),y
        beq !+
        iny
        bne !-
!:      iny
        tya
        clc
        adc cmenu_item_adr
        bcc !+
        inc cmenu_item_adr+1
!:      sta cmenu_item_adr
        inx
        jmp fmi1
fmi2:   rts

find_next_menu_item:
        ldx cmenu_item_idx
        inc cmenu_item_idx
        jmp fmi1

//--------------------------------
// menu setup
//--------------------------------

// setup all current menu variables for menu whose index is in current_menu
setup_menu:
        ldx current_menu
        ldy menu_items_no,x
        sty cmenu_items_no
        tya
        sec
        sbc menu_height,x
        sta cmenu_max_first
        ldy menu_width,x
        sty cmenu_width
        dey
        dey
        dey
        sty cmenu_iwidth        // menu_width-3 (width of key display)
        ldy menu_height,x
        sty cmenu_height
        ldy menu_spacing,x
        sty cmenu_spacing
        txa                     // offsets are 2-byte
        asl
        tax
        lda menu_offset,x
        sta cmenu_offset
        lda menu_offset+1,x
        sta cmenu_offset+1
        lda menu_items,x
        sta cmenu_items
        lda menu_items+1,x
        sta cmenu_items+1
        // calculate offset for menu items
        lda cmenu_offset+1
        sta cmenu_offset_it+1
        lda cmenu_offset
        clc
        adc #44                 // 1 line + 3 chars for key display
        sta cmenu_offset_it
        bcc !+
        inc cmenu_offset_it+1
        // calculate offset for menu item keys
!:      lda cmenu_offset+1
        sta cmenu_offset_key+1
        lda cmenu_offset
        clc
        adc #42                 // 1 line + 2
        sta cmenu_offset_key
        bcc !+
        inc cmenu_offset_key+1

!:      lda menu_names,x
        sta chrmem
        lda menu_names+1,x
        sta chrmem+1
        ldy #0
!:      lda (chrmem),y
        ora #$80
        sta SCREEN,y
        lda menu_help,y
        ora #$80
        sta SCREEN+24*40,y
        iny
        cpy #40
        bne !-
        rts

//--------------------------------
// utility functions
//--------------------------------

// X - char, Y - color
clear_screen:
        txa
        ldx #0
!:      sta SCREEN,x
        sta SCREEN+$100,x
        sta SCREEN+$200,x
        sta SCREEN+$2E8,x
        pha
        tya
        sta COLORR,x
        sta COLORR+$100,x
        sta COLORR+$200,x
        sta COLORR+$2E8,x
        pla
        inx
        bne !-
        rts

// returns key as read by GETIN, makes a small pause before reading
read_key:
        ldy #40
        ldx #0
!:      dex
        bne !-
        dey
        bne !-
!:      jsr SCNKEY
        jsr GETIN
        beq !-
        rts

// find first active drive
FindFirstDrive:
        lda #1
        sta DriveFound
        lda #8
        sta drive
!:      ldx #0
        stx status
        jsr LISTEN
        lda #$FF
        jsr SECOND
        lda status
        bpl !+
        jsr UNLSTN
        inc drive
        lda drive
        cmp #31
        bne !-
        lda #8          // if not found, set 08
        sta drive
        dec DriveFound
!:      jsr UNLSTN
        rts

//--------------------------------
// IRQ
//--------------------------------

// setup VIC II for waving menu
WaveVIC:
        sei
        lda #<WaveIRQ
        sta IRQ_Vector
        lda #>WaveIRQ
        sta IRQ_Vector+1

        ldy #$00        // it must be copied to RAM, as cartridge is turned off on IRQ
!:      lda C128IRQRAM, y
        sta C128IRQRAMLoc,y
        iny
        cpy #C128IRQRAMLen
        bne !-
/*
        lda #$7f        //disable cia irq
        sta $dc0d
        sta $dd0d
        lda $dc0d
        lda $dd0d
*/
        lda $d01a       //enable raster irq
        ora #01
        sta $d01a
        lda $d011       //raster irq bit 8
        and #$7f
        sta $d011
        lda #SCR_FirstLine
        sta $d012
        sta CurrentLine
        lda #06
        sta $d020
        lda #00
        sta $d021
        sta CurrentWave
        sta FirstWave
        sta WavePause
/*
        lda $dd00 //vic bank
        and #$fc
        ora #00   !0-c000,1-8000
        sta $dd00
        //vic setup $d018
        //bit 7-4 screen  0001-$0400
        //bit 3-1 charmem  001-$0800
        lda #%00010100
        sta $d018
*/
        cli
        rts

// return VIC II to normal state
NormalVIC:
        sei
        lda #<IRQ_Kernel
        sta IRQ_Vector
        lda #>IRQ_Kernel
        sta IRQ_Vector+1

//        lda $d01a
//        and #$fe
        lda #$F1
        sta $d01a
        lda #$c8
        sta $d016
        lda #$15    // upper chars
        sta $D018

        lda RunBorderColor
        sta $d020
        lda RunPaperColor
        sta $d021
        lda RunInkColor
        sta CursorColor
        ldx #$20  // clear SID registers
        lda #$00
!:      sta $D400,x
        dex
        bpl !-
        cli
        rts

C128IRQRAM:
.pseudopc C128IRQRAMLoc {
// IRQ for menu waves
WaveIRQ:
        //inc $d020 //debug
        lda DoWaveRAM
        beq irqend
//        nop;nop;nop;nop;nop;nop
        ldy CurrentWave
        lda WaveTable,y
        sta $d016
        iny
        cpy #WaveTableMax
        bne irq0
        ldy #00
irq0:   sty CurrentWave
        ldy CurrentLine
        cpy #SCR_LastLine-8
        bcc !+
        dey
!:      tya
        clc
        adc #08
        cmp #SCR_LastLine
        bcc irq1
        lda #SCR_FirstLine
irq1:   sta CurrentLine
        sta $d012       // this line is how they create a crazy number of raster interrupts
        cmp #SCR_FirstLine
        bne irqend
        lda #$c8
        sta $d016
        lda FirstWave
        sta CurrentWave
        ldy WavePause
        iny
        cpy #WaveSpeed
        bne irq3
        ldy #00
        ldx FirstWave
        inx
        cpx #WaveTableMax
        bne irq4
        ldx #00
irq4:   stx FirstWave
irq3:   sty WavePause

irqend: ldy $d019
        sty $d019   // clear (ack) interrupt

        // dec $d020    // debug

        lda CurrentLine
        cmp #SCR_FirstLine
        bne end2            // keep the interrupt on the animation for now...

        lda DoSoundRAM
        cmp #$00
        beq end2
        cmp #$02
        beq domusic
        //jmp domusic

dosound:
        ldx #$50
!:      lda $D41B    //Oscillator 3 Output
        sta $D401    //Voice 1: Frequency Control - High-Byte
        dex
        bne !-
        beq end2

domusic:
        //dec $d020    // debug
        // set up next interrupt for music
        lda #<musicirq
        sta IRQ_Vector
        lda #>musicirq
        sta IRQ_Vector+1
        lda $d011  // b7 is 9th bit of scan line value
        and #$7f
        sta $d011   // clear b7
        lda #SCR_LastLine + 10  // lower 8 bits of scan line value
        sta $d012

end2:   pla
        sta $FF00
        pla;tay;pla;tax;pla
        rti

musicirq:
        // dec $d020    // show raster time consumed
        inc $d019  // clear (ack) interrupt
        jsr sidPlay  // update music

        // set next interrupt for animation
        lda #<WaveIRQ
        sta IRQ_Vector
        lda #>WaveIRQ
        sta IRQ_Vector+1
        lda $d011  // b7 is 9th bit of scan line value
        and #$7f
        sta $d011   // clear b7
        lda #SCR_FirstLine  // lower 8 bits of scan line value
        sta $d012
        jmp end2

WaveTable: .byte $c0,$c0,$c1,$c1,$c2,$c3,$c4,$c5,$c5,$c6,$c6,$c7,$c7
           .byte $c7,$c7,$c6,$c6,$c5,$c5,$c4,$c3,$c2,$c1,$c1,$c0,$c0
.label WaveTableMax = *-WaveTable
}
.label C128IRQRAMLen = *-C128IRQRAM

//--------------------------------
// memory copy
//--------------------------------

.label TableAddress  = $5C
.label ProgramIndex  = $5E

prepare_run:
        sta ProgramIndex
        ldy #0
!:      cpy current_menu
        beq !+
        ldx menu_items_no,y
        txa
        clc
        adc ProgramIndex
        sta ProgramIndex
        iny
        bne !-
!:      jsr NormalVIC

        // restore 1000-1300 from 3000 (music goes to 1000)
        //lda #$0A        // MMU - turn on KERNAL
        //sta $FF00
        lda #00
        sta zpTmp1
        sta zpTmp2
        lda #$30
        sta zpTmp1+1
        lda #$10
        sta zpTmp2+1
        ldx #$03
        ldy #00
!:      lda (zpTmp1),y
        sta (zpTmp2),y
        iny
        bne !-
        inc zpTmp1+1
        inc zpTmp2+1
        dex
        bne !-

        ldx #$20
        ldy RunInkColor
        jsr clear_screen
        jsr FindFirstDrive


startCopy:
        ldy #CartCopyLen    // copy routine to CartCopyAddress
!:      lda CartCopyCode,y
        sta CartCopyAddress,y
        dey
        cpy #$FF
        bne !-
        ldy #0              //calculate program table element address
        sty TableAddress+1  //each table element is 9 bytes
        lda ProgramIndex    //ProgramIndex*8
        asl
        rol TableAddress+1
        asl
        rol TableAddress+1
        asl
        rol TableAddress+1
        sta TableAddress
        lda ProgramIndex    //ProgramIndex*8 + ProgramIndex
        clc
        adc TableAddress
        sta TableAddress
        bcc sc2
        inc TableAddress+1
sc2:    lda ProgramTable    //add Program table address
        clc
        adc TableAddress
        sta TableAddress
        lda ProgramTable+1
        adc TableAddress+1
        sta TableAddress+1
        lda (TableAddress),y    //set values in copy routine
        sta CartBank
        iny;lda (TableAddress),y
        sta CartAddr
        iny;lda (TableAddress),y
        sta CartAddr+1
        iny;lda (TableAddress),y
        sta PrgLenLo
        iny;lda (TableAddress),y
        sta PrgLenHi
        iny;lda (TableAddress),y
        sta MemAddr
        iny;lda (TableAddress),y
        sta MemAddr+1
        iny;lda (TableAddress),y
        sta pstart+1
        iny;lda (TableAddress),y
        sta pstart+2
        beq sc3     //if high byte of start address != 0
        //lda #$4c    //change lda to jmp
        lda #$20    //change lda to jsr
        sta pstart  //else, run as basic
sc3:    jmp CartCopyAddress

CartCopyCode:
.pseudopc CartCopyAddress {
        jmp !++
        // return to basic
        lda DriveFound      // if there was no drive detected, skip autoboot
        beq !+
        jsr $7335           // boot f88c
        jsr $51d6           // NEW command
!:      jsr $419b           // display power-on message
        jmp $51d6           // NEW command again (otherwise something isn't initialized properly)
!:      sei
        lda #$0a            // turn on I/O at $D000
        sta $ff00
        lda $d503
        pha
        lda $d502
        pha
        lda $d501
        pha
        lda #$0a
        sta $d501           // PCRA preconfig A = cartridge + I/O (bank switch)
        lda #$3f
        sta $d502           // PCRB preconfig B = all RAM bank 0 (write)
#if BANK_32K
        lda #$2B            // external ROM at $8000-$FFFF, no I/O, RAM bank 0
#else
        lda #$0a
#endif
        sta $d503           // PCRC preconfig C = cartridge no I/O (read)
        lda #$0b
        sta $d011           // disable VIC screen
        lda #$01
        sta $d030           // enable 2MHz mode
        ldx PrgLenLo:#00    // program length lo
        ldy PrgLenHi:#00    // program length hi
cbank:
        lda CartBank:#00    //cartridge start bank
#if BANK_32K
        asl                 // bank number *4 on 32K version
        asl
#endif
        sta $ff01	    // LCRA: turn on I/O
        sta $de00           // cartridge bank switching address
caddr:
        sta $ff03           // LCRC: cartridge only
        lda CartAddr:$8200  //cartridge start adr
        sta $ff02	    // LCRB: all RAM
        sta MemAddr:$1C01   //c128 memory start adr
        dex
        cpx #$ff
        bne cc1
        dey
        cpy #$ff
        beq crtoff
cc1:    inc MemAddr
        bne cc2
        inc MemAddr+1
cc2:    inc CartAddr
        bne caddr
        inc CartAddr+1
        lda CartAddr+1
#if BANK_32K
	cmp #$ff	// page $FF00 with MMU?
        bne caddr
#else
        cmp #$c0        // next bank?
        bne caddr
#endif
        inc CartBank
        lda #$80        // cartridge bank is on $8000-$9fff
        sta CartAddr+1
        jmp cbank
crtoff:
        sta $FF01	// LCRA: turn on I/O
        lda #$00        // set cartridge to first bank
        sta $de00
        sta $d030       // disable 2MHz mode
        lda #$1b
        sta $d011       // enable VIC screen
        pla		// restore PCRA/PCRB/PCRC
        sta $d501
        pla
        sta $d502
        pla
        sta $d503

        ldx #$FF
        txs
        cld
        lda #$E3
        sta $01
        lda #$37
        sta $00

                              // BIT 0   : $D000-$DFFF (0 = I/O Block)
                              // BIT 1   : $4000-$7FFF (0 = System ROM, Basic Lo)
                              // BIT 2/3 : $8000-$BFFF (10 = External ROM)
                              // BIT 4/5 : $C000-$CFFF/$E000-$FFFF (00 = Kernal ROM)
                              // BIT 6/7 : RAM used. (00 = RAM 0)
        lda #$00
        sta $FF00       // MMU Configuration Register

        ldx MemAddr     //set end of program (var start)
        inx
        stx $1210
        ldx MemAddr+1   //set end of program (var start)
        stx $1211

        lda #$00        // turn on screen editor
        sta $00D8

.label  RUN_PROGRAM = $AF99
.label  JUMP_FAR     = $02F2
pstart: lda $1C01
        jsr $4F4F               // relink
        lda #>RUN_PROGRAM       // BASIC - run program
        pha                     //
        lda #<RUN_PROGRAM       //
        pha                     //
        lda #0
        pha
        jmp JUMP_FAR            // this will change to the kernal/BASIC bank 
}
.label CartCopyLen = *-CartCopyCode

//--------------------------------
// menu sound init
//--------------------------------

sound_init:
        lda #$00
        sta $D40F    //Voice 3: Frequency Control - High-Byte
        lda #$1E
        sta $D40E    //Voice 3: Frequency Control - Low-Byte
        lda #$F0
        sta $D414    //Voice 3: Sustain / Release Cycle Control
        sta $D406    //Voice 1: Sustain / Release Cycle Control
        lda #$81
        sta $D412    //Voice 3: Control Register
        lda #$11
        sta $D404    //Voice 1: Control Register
        lda #$83
        sta $D418    //Select Filter Mode and Volume
        rts

music_init:
        sei
        // since self-modifying code not allowed in ROM, this has to use 4 ZP locs
move1:  lda sidStartAdr
        sta zpTmp1           // zpTmp1 is source pointer
        lda sidStartAdr+1
        sta zpTmp1+1
        lda #<sidStart
        sta zpTmp2           // zpTmp2 is dest pointer
        lda #>sidStart
        sta zpTmp2+1

        ldy #$00
        ldx #$00             // x = count of pages moved (1K = 4 pages)
move2:  lda (zpTmp1),y
        sta (zpTmp2),y
        iny
        bne move2
        inc zpTmp1+1
        inc zpTmp2+1
        inx
        cpx sidPages         // stop after all pages moved
        bne move2
        cli
        lda sidSubtune       // select subtune
        jsr sidInit          // init music
        rts

//--------------------------------
// menu data
//--------------------------------

.encoding "screencode_mixed"

// menu chars corners
.const MENU_CHR_UL   = $EC
.const MENU_CHR_UR   = $FB
.const MENU_CHR_DL   = $FC
.const MENU_CHR_DR   = $FE
.const MENU_COL_UL   = 10
.const MENU_COL_UR   = 10
.const MENU_COL_DL   = 10
.const MENU_COL_DR   = 10

// menu chars sides
.const MENU_CHR_L       = $61
.const MENU_CHR_R       = $E1
.const MENU_CHR_U       = $E2
.const MENU_CHR_D       = $62
.const MENU_COL_L       = 10
.const MENU_COL_R       = 10
.const MENU_COL_U       = 10
.const MENU_COL_D       = 10

// menu chars misc
.const MENU_CHR_BACK    = $20
.const MENU_CHR_SHAD    = $E6
.const MENU_COL_BACK    = 5
.const MENU_COL_SHAD    = 2
.const MENU_COL1_KEY    = 12
.const MENU_COL2_KEY    = 15
.const MENU_COL_NAME    = 4
.const SCREEN_CHR       = $A0
.const SCREEN_COL       = 6

// menu border first, middle, bottom and last line chars
menu_border_chr:
        .byte MENU_CHR_UL,MENU_CHR_U,MENU_CHR_UR,SCREEN_CHR
        .byte MENU_CHR_L,MENU_CHR_BACK,MENU_CHR_R,MENU_CHR_SHAD
        .byte MENU_CHR_DL,MENU_CHR_D,MENU_CHR_DR,MENU_CHR_SHAD
        .byte SCREEN_CHR,MENU_CHR_SHAD,MENU_CHR_SHAD,MENU_CHR_SHAD

// menu border first, middle, bottom and last line colors
menu_border_col:
        .byte MENU_COL_UL,MENU_COL_U,MENU_COL_UR,SCREEN_COL
        .byte MENU_COL_L,MENU_COL_BACK,MENU_COL_R,MENU_COL_SHAD
        .byte MENU_COL_DL,MENU_COL_D,MENU_COL_DR,MENU_COL_SHAD
        .byte SCREEN_COL,MENU_COL_SHAD,MENU_COL_SHAD,MENU_COL_SHAD

// misc menu chars
menu_chr_back:  .byte MENU_CHR_BACK
menu_key_chr:   .byte $F5,$B1,$F6
menu_key_col:   .byte MENU_COL1_KEY,MENU_COL2_KEY,MENU_COL1_KEY

// keys for selecting a program
menu_keys:      .fill 9,i+$31   // 1-9
                .byte $30       // 0
                .fill 26,i+$41  // a-z
                .fill 26,i+$C1  // A-Z
.label menu_keys_no = *-menu_keys
                .byte $5F       // left arrow

*=* "Menu Data" virtual

sidStartAdr:    .word $0000
sidPages:       .byte $06   // number of pages (256 bytes) this SID takes up
sidSubtune:     .byte $00   // SIDs can possibly contain multiple songs

ProgramTable:   .word %0000

// program run colors
RunBorderColor: .byte 12 //14
RunPaperColor:  .byte 15 //6
RunInkColor:    .byte 0  //14
DoWave:         .byte 1  //menu waving
DoSound:        .byte 1  //menu sound

// menu data for 8 menus
menu_items_no:  .byte 1,0,0,0,0,0,0,0
menu_offset:    .word 85,0,0,0,0,0,0,0
menu_width:     .byte 15,0,0,0,0,0,0,0
menu_height:    .byte 10,0,0,0,0,0,0,0
menu_spacing:   .byte 1,0,0,0,0,0,0,0

menu_names:     .word menu_name1,0,0,0,0,0,0,0
menu_items:     .word menu_items1,0,0,0,0,0,0,0

menu_help:      .text "(Shift)CRSR: Scroll, Fn/Ret: Menu select"

menu_name1:     .text "            Testing...                  "

menu_items1:
        .text @"Testing1\$00"
        .text @"Testing2\$00"

programs:
// program table
// .byte bank
// .word address in bank
// .word length
// .word load address
// .word start address
//     if hi byte=0, then run as basic

// not working with menu
// f4/6 64tester
// f4/c basic 64 compiler
// f5/e turbocopy 5
// f5/h turbo nibbler 5

sidToMove:
//.import binary "HelloWorld.sid", 126  // skip the first 126 bytes

