#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#    Magic Cartridge Generator (c) 2013-2024  Žarko Živanov
#    32k banks/2MHz code by Maciej 'YTM/Elysium' Witkowiak
#    Cartridge schematics and PCB design (c) 2013-2022 Marko Šolajić
#    E-mails: zzarko and msolajic at gmail

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from __future__ import division

VERSION="3.8"

import copy
import array
import os
import glob
import sys
import re

if sys.version_info >= (3, 0):
    import configparser
else:
    import ConfigParser as configparser

#############################################################
#               Computer-dependant settings
#############################################################

C64Config = {
    'computer' : "c64",
    'description' : "Commodore 64 Magic Desk/Gmod2",
    'run' : 0x0801,
    'basic' : 0xFCE2,
    'help' : "(Shift)CRSR: Scroll, Fn/Ret: Menu select",
    'romlast' : 0x9FFF,
    'romsize' : 0x2000,
    'menu' : "Commodore 64 Magic Desk Cartridge",
    'menuprg' : 'menu-c64',
    'musicglob' : "*.[sS][iI][dD]",
    'musicext' : ".sid",
    'cartsizes' : [64,128,256,512,1024],
    'color_border' : 14,
    'color_background' : 6,
    'color_character' : 14,
    'bank_bytes' : 1,
}

C64GM3Config = {
    'computer' : "c64gm3",
    'description' : "Commodore 64 Gmod3",
    'run' : 0x0801,
    'basic' : 0xFCE2,
    'help' : "(Shift)CRSR: Scroll, Fn/Ret: Menu select",
    'romlast' : 0x9FFF,
    'romsize' : 0x2000,
    'menu' : "Commodore 64 Gmod3 Cartridge",
    'menuprg' : 'menu-c64-gm3',
    'musicglob' : "*.[sS][iI][dD]",
    'musicext' : ".sid",
    'cartsizes' : [64,128,256,512,1024,2048,4096],
    'color_border' : 14,
    'color_background' : 6,
    'color_character' : 14,
    'bank_bytes' : 2,
}

C128Config = {
    'computer' : "c128",
    'description' : "Commodore 128 Magic 128/Gmod2",
    'run' : 0x1C01,
    'basic' : 0x0803,   # check CartCopyCode for details
    'help' : "(Shift)CRSR: Scroll, Fn/Ret: Menu select",
    'romlast' : 0xBFFF,
    'romsize' : 0x4000,
    'menu' : "Commodore 128 Magic Cartridge",
    'menuprg' : 'menu-c128',
    'musicglob' : "*.[sS][iI][dD]",
    'musicext' : ".sid",
    'cartsizes' : [16,32,64,128,256,512,1024],
    'color_border' : 13,
    'color_background' : 11,
    'color_character' : 13,
    'bank_bytes' : 1,
}

C12832KConfig = {
    'computer' : "c128",
    'description' : "Commodore 128 External ROM (32K)",
    'run' : 0x1C01,
    'basic' : 0x0803,   # check CartCopyCode for details
    'help' : "(Shift)CRSR: Scroll, Fn/Ret: Menu select",
    'romlast' : 0xFEFF, # FFFF but page FF00 is always skipped over - here and in the menu code
    'romsize' : 0x7F00,
    'menu' : "Commodore 128 Magic Cartridge",
    'menuprg' : 'menu-c128-32k',
    'musicglob' : "*.[sS][iI][dD]",
    'musicext' : ".sid",
    'cartsizes' : [16,32,64,128,256,512,1024],
    'color_border' : 13,
    'color_background' : 11,
    'color_character' : 13,
    'bank_bytes' : 1,
}

Plus4Config = {
    'computer' : "plus4",
    'description' : "Commodore Plus/4 Magic 264",
    'run' : 0x1001,
    'basic' : 0x8A7B,
    'help' : "Cursor: Scroll, Fn/Help/Ret: Menu select",
    'romlast' : 0xBFFF,
    'romsize' : 0x4000,
    'menu' : "Commodore 264 Magic Cartridge",
    'menuprg' : 'menu-plus4',
    'musicglob' : "*.[bB][iI][nN]",
    'musicext' : ".bin",
    'cartsizes' : [8,16,32,64,128,256,512,1024,2048],
    'color_border' : 110, #238,
    'color_background' : 113, #241,
    'color_character' : 16,
    'bank_bytes' : 1,
}

#############################################################
#               Usage and startup messages
#############################################################

def usage(code=0):
    print("""Usage:
Generating cartridge without configuration file:
    python {0} <computer> [prg directory]
     - Scans the prg directory and places all prg files on cartridge
       (by default, it scans 'prg' directory)
       Parameter <computer> is mandatory and can be:
        -c - Commodore 64
        -3 - Commodore 64 Gmod3
        -p - Plus/4
        -8 - Commodore 128
     - Cartridge name will be the same as prg directory name,
        if one is supplied

Generating cartridge with configuration file:
    python {0} <cfg_file> (with or without cfg extension)

Generating configuration file:
    python {0} <computer> [prg directory/cfg file name]
       Parameter <computer> is mandatory and can be:
        -gc - Commodore 64
        -g3 - Commodore 64 Gmod3
        -gp - Plus/4
        -g8 - Commodore 128
   By default, script scans 'prg' directory. Configuration name will
     be the same as prg directory name, if one is supplied.


All programs for cartridge must be in prg format!

Create prg directory if it doesn't exist and place all prg files
for cartridge inside. To make specific program order, you can add
NNN_ prefix to prg files (N - 0-9). This prefix will be excluded
from name in menu. Or, you can use CFG file to define cartridge.
Sample CFG file is provided with installation.

Generated C64 bin file can be burned to Magic Desk compatibile
eprom/cartridge, or to Gmod2 cartridge is the size is 512K or less,
or can be used with VICE after converting to CRT.

Generated C64 Gmod3 bin file can be burned to Gmod3 or compatibile
eprom/cartridge, or can be used with VICE after converting to CRT.

Generated Plus/4 bin file can be burned to 264 Series Magic
eprom/cartridge, or can be used with VICE after converting to CRT.

Generated C128 bin file can be burned to C128 Magic eprom/cartridge,
or to Gmod2 cartridge is the size is 512K or less, or used with
latest VICE (3.8 or later).
""".format(sys.argv[0]))
    sys.exit(code)

def error(s, usg=False):
    print("\nERROR!! {0}\n".format(s))
    if (usg):
        usage(1)
    else:
        sys.exit(1)

print(u"\nMagic Cartridge Generator v%s\n(c) 2013-2022  Žarko Živanov" % VERSION)
print(u"\nMagic Desk Compatible Cartridge, Magic 264 Cartridge, Magic 128 Cartridge \n(c) 2013-2022  Marko Šolajić")

#############################################################
#       PETSCII and menu constants and dictionaries
#############################################################

#commodore 64 screen codes
SCR_CODES="""@abcdefghijklmnopqrstuvwxyz[|]|| !"#$%&'()*+,-./0123456789:;<=>?|ABCDEFGHIJKLMNOPQRSTUVWXYZ"""
SCR_DICT={SCR_CODES[pos]:pos for pos in range(len(SCR_CODES))}
#commodore 64 petascii codes
PETASCII_CODES="""0123456789|||||||abcdefghijklmnopqrstuvwxyz||||<|ABCDEFGHIJKLMNOPQRSTUVWXYZ"""
PETASCII_DICT={PETASCII_CODES[pos]:pos+48 for pos in range(len(PETASCII_CODES))}

#############################################################
#                   Cartridge settings
#############################################################

#menu limits
MAX_MENU_ITEMS = 10+26*2
MAX_MENUS = 8
#cartridge size (8/16/32/64/128/256/512k/1024k/2048k, 0 - autosize)
cart_sizek = 0
cart_size = cart_sizek*1024
#target computer
computer = "c64"
#directory for prg files
prg_directory  = "prg"
#finished cartridge bin file
cart_file_name = "compilation.bin"
#order of programs in CFG file
cart_order = []
#border color after program select
color_border = 14
#background color after program select
color_background = 6
#character color after program select
color_character = 14
#should menu wave
cart_wave = 1
#should menu have ROSS cartridge sound (1) or GoatTracker music (2)
cart_sound = 0
#sid file name
cart_sid_file = ""
#sid song number
cart_sid_song = 0
#should cfg file be generated
generate_cfg = False

#############################################################
#                   Parameter parsing
#############################################################

CompConfig = C64Config

if (len(sys.argv) > 3) or (len(sys.argv) < 2):
    usage();
if (sys.argv[1] == "-gc") or (sys.argv[1] == "-gp") or (sys.argv[1] == "-g8") or (sys.argv[1] == "-g832") or (sys.argv[1] == "-g3"):
    generate_cfg = True
    if len(sys.argv) == 3:
        prg_directory = os.path.dirname(sys.argv[2])
        if prg_directory == "":
            prg_directory = os.path.basename(sys.argv[2])
        C64Config['menu'] = prg_directory
        C64GM3Config['menu'] = prg_directory
        Plus4Config['menu'] = prg_directory
        C128Config['menu'] = prg_directory
        C12832KConfig['menu'] = prg_directory
        cfgfile = prg_directory + ".cfg"
        #if cfgfile[-4:] != ".cfg" and cfgfile[-4:] != ".CFG":
        #    cfgfile += ".cfg"
    else:
        cfgfile = "output.cfg"
    if sys.argv[1] == "-gc":
        CompConfig = C64Config
    elif sys.argv[1] == "-g3":
        CompConfig = C64GM3Config
    elif sys.argv[1] == "-g8":
        CompConfig = C128Config
    elif sys.argv[1] == "-g832":
        CompConfig = C12832KConfig
    else:
        CompConfig = Plus4Config
elif (sys.argv[1] == "-c") or (sys.argv[1] == "-p") or (sys.argv[1] == "-8") or (sys.argv[1] == "-832") or (sys.argv[1] == "-3"):
    if len(sys.argv) == 3:
        prg_directory = os.path.dirname(sys.argv[2])
        if prg_directory == "":
            prg_directory = os.path.basename(sys.argv[2])
        C64Config['menu'] = prg_directory
        C64GM3Config['menu'] = prg_directory
        C128Config['menu'] = prg_directory
        C12832KConfig['menu'] = prg_directory
        Plus4Config['menu'] = prg_directory
        cart_file_name = prg_directory + ".bin"
        #if cart_file_name[-4:] != ".bin" and cart_file_name[-4:] != ".BIN":
        #    cart_file_name += ".bin"
elif sys.argv[1][0] == '-':
    error("Unknown option '%s'" % sys.argv[1])

if (sys.argv[1] == "-p") or (sys.argv[1] == "-gp"):
    CompConfig = Plus4Config
elif (sys.argv[1] == "-8") or (sys.argv[1] == "-g8"):
    CompConfig = C128Config
elif (sys.argv[1] == "-832") or (sys.argv[1] == "-g832"):
    CompConfig = C12832KConfig
elif (sys.argv[1] == "-3") or (sys.argv[1] == "-g3"):
    CompConfig = C64GM3Config

#cartridge menu program
cart_menu_file = CompConfig['menuprg']
#default menu help text
cart_help = CompConfig['help']

#############################################################
#                   Helper functions
#############################################################

#returns array of screen codes
def to_screen(s):
    return [SCR_DICT[x] for x in s]

#returns the size of an open file
def flen(f):
    return os.fstat(f.fileno()).st_size

#adds one byte to array
def append_byte(arr,byte):
    arr.append(byte & 0xff)

#adds two bytes to array (lo/hi)
def append_word(arr,word):
    arr.append(word & 0xff)
    arr.append(word >> 0x08)

def append_text(bytearray, text, zero=False):
    text = to_screen(text)
    for c in text:
        append_byte(bytearray, c)
    if zero:
        append_byte(bytearray, 0)

def center_text(text, width=40):
    l = len(text)
    pos = (40 - l) // 2
    text = " "*pos + text + " "*(width-pos-len(text))
    return text

def genitem(name, prg="", run=0, load=0, length=0, data=0):
    return {'name':name[:31], 'prg':prg, 'run':run, 'load':load, 'len': length, 'data':data, 'link': None}

def genmenu(title, order=None, spacing=0, width=0, height=0, x=0, y=0):
    return {'title':center_text(title), 'order':order, 'spacing':spacing, 'items':[],
        'width':width, 'height':height, 'x':x, 'y':y, 'offset':0}

def printmenus():
    print("\nMENUS:")
    for menuid in sorted(menus):
        menu = menus[menuid]
        print("MENU",menuid,menu['title'],"LEN",len(menu['items']),"W",menu['width'],"H",menu['height'])
        for i,item in enumerate(menu['items']):
            print("    ITEM",i,item['name'],"LINK",item['link'])

#############################################################
#               Configuration file parsing
#############################################################

menus = {}
programs = {}
prgs_found = 0

if (len(sys.argv) == 2) and (sys.argv[1][0] != '-'):
    #read configuration from cfg file
    cfgfile = sys.argv[1]
    if cfgfile[-4:] != ".cfg" and cfgfile[-4:] != ".CFG":
        cfgfile += ".cfg"
    cart_file_name = "%s.bin" % cfgfile[:-4]
    if os.access(cfgfile, os.F_OK):
        print("\nReading configuration file %s ..." % cfgfile)
        if sys.version_info >= (3, 0):
            cfg = configparser.ConfigParser()
        else:
            cfg = configparser.SafeConfigParser()
        cfg.read(cfgfile)
        programs = cfg.sections()
        if cfg.has_section("cartridge"):
            print("Reading cartridge section ...")
            if cfg.has_option("cartridge","prgdir"):
                prg_directory = cfg.get("cartridge","prgdir")
            if cfg.has_option("cartridge","computer"):
                computer = cfg.get("cartridge","computer")
                if computer == "c64":
                    CompConfig = C64Config
                elif computer == "c64gm3":
                    CompConfig = C64GM3Config
                elif computer == "plus4":
                    CompConfig = Plus4Config
                elif computer == "c128":
                    CompConfig = C128Config
                elif computer == "c128_32":
                    CompConfig = C12832KConfig
                else:
                    error("Unknown 'computer' option defined: %s" % computer)
                cart_menu_file = CompConfig['menuprg']
                cart_help = CompConfig['help']
            else:
                error("Configuration must have 'computer' option defined!")
            if cfg.has_option("cartridge","bin"):
                cart_file_name = cfg.get("cartridge","bin")
                if cart_file_name[-4:] != ".bin" and cart_file_name[-4:] != ".BIN":
                    cart_file_name += ".bin"
            if cfg.has_option("cartridge","size"):
                cart_sizek = cfg.getint("cartridge","size")
                if cart_sizek not in CompConfig['cartsizes']:
                    error("Wrong cartridge size, allowed are: " + CompConfig['cartsizes'])
            if cfg.has_option("cartridge","menu"):
                cart_menu_file = cfg.get("cartridge","menu")
#                if cart_menu_file[-4:] == ".prg" or cart_menu_file[-4:] == ".PRG":
#                    cart_menu_file = cart_menu_file[:-4]
            if cfg.has_option("cartridge","border"):
                color_border = cfg.getint("cartridge","border")
                if color_border not in range(16):
                    error("Border color must be 0-15")
            if cfg.has_option("cartridge","background"):
                color_background = cfg.getint("cartridge","background")
                if color_background not in range(16):
                    error("Background color must be 0-15")
            if cfg.has_option("cartridge","character"):
                color_character = cfg.getint("cartridge","character")
                if color_character not in range(16):
                    error("Character color must be 0-15")
            if cfg.has_option("cartridge","help"):
                cart_help = cfg.get("cartridge","help")
                if len(cart_help) > 40:
                    error("Cartridge help text must be 40 characters or less")
            if cfg.has_option("cartridge","wave"):
                cart_wave = int(cfg.get("cartridge","wave"))
            if cfg.has_option("cartridge","sound"):
                cart_sound = int(cfg.get("cartridge","sound"))
            if cfg.has_option("cartridge","music"):
                cart_sid_file = os.path.join(prg_directory,cfg.get("cartridge","music"))
            if cfg.has_option("cartridge","sid"):
                cart_sid_file = os.path.join(prg_directory,cfg.get("cartridge","sid"))
            if cfg.has_option("cartridge","song"):
                cart_sid_song = int(cfg.get("cartridge","song"))
            programs.remove("cartridge")

        for m in range(1,MAX_MENUS+1):
            menu = "menu%d" % m
            if cfg.has_section(menu):
                print("Reading menu %d section ..." % m)
                title = ""
                order = ["{:03d}".format(i) for i in range(100*m,100*m+100)]
                spacing = width = height = x = y = 0
                if cfg.has_option(menu,"order"):
                    order = cfg.get(menu,"order")
                    order = order.split(",")
                    order = [ "%d%s" % (m, x) for x in order]
                if cfg.has_option(menu,"title"):
                    title = cfg.get(menu,"title")
                    if len(title) > 40:
                        error("Menu %d title must be 40 characters or less" % m)
                if cfg.has_option(menu,"spacing"):
                    spacing = int(cfg.get(menu,"spacing"))
                if cfg.has_option(menu,"width"):
                    width = int(cfg.get(menu,"width"))
                if cfg.has_option(menu,"height"):
                    height = int(cfg.get(menu,"height"))
                if cfg.has_option(menu,"x"):
                    x = int(cfg.get(menu,"x"))
                if cfg.has_option(menu,"y"):
                    y = int(cfg.get(menu,"y"))
                menus["%d" % m] = genmenu(title, order, spacing, width, height, x, y)
                programs.remove(menu)
        if len(menus) == 0:
            order = ["{:03d}".format(i) for i in range(100,200)]
            menus['1'] = genmenu("",order,0)

        for menuid in sorted(menus):
            menu = menus[menuid]
            print("Menu %s programs:" % menuid)
            for prgno in menu['order']:
                prgsec = "prg"+prgno
                if not cfg.has_section(prgsec):
                    continue
                prgname = cfg.get(prgsec,"file")
                if prgname[-4:] == ".prg" or prgname[-4:] == ".PRG":
                    prgname = prgname[:-4]
                if cfg.has_option(prgsec,"name"):
                    name = cfg.get(prgsec,"name")
                else:
                    name = prgname[:-4].replace("_"," ")
                if cfg.has_option(prgsec,"run"):
                    runaddr = cfg.get(prgsec,"run")
                    if runaddr[0] == '$': runaddr = runaddr.replace("$","0x")
                    runaddr = int(runaddr,0)
                else:
                    runaddr = 0
                menu['items'].append(genitem(name,prgname,runaddr))
                print("    %s" % name)
                programs.remove(prgsec)
                prgs_found += 1
    else:
        error("CFG file '{0}' not found.".format(cfgfile), True)

print(u"\nUsing configuration %s, %s" % (CompConfig['computer'], CompConfig['description']))

if not os.path.isdir(prg_directory):
    error("Prg directory '%s' not found!" % prg_directory)

#############################################################
#                   PRG directory parsing
#############################################################

if len(menus) == 0 or prgs_found == 0:
    #collect all prg files in prg directory
    print("\nReading prg files from prg directory ...")
    prgList = glob.glob( os.path.join(prg_directory, '*.[pP][rR][gG]') )
    if len(prgList) == 0:
        error("No prg files found. Make sure to place them in prg directory.", True)
    sidList = glob.glob( os.path.join(prg_directory, CompConfig['musicglob']) )
    if len(sidList) != 0:
        cart_sid_file = sidList[0]
        cart_sound = 2
        print("Found music file: %s" % os.path.basename(cart_sid_file)[:-4])
    prefix = re.compile("\A([0-9])([0-9]?[0-9]?)_(.*)")
    suffix = re.compile("(.*)_([0-9]+|0[xX][0-9a-fA-F]+)\Z")
    prgList.sort()
    print("Found prg files:")
    for prg in prgList:
        prgname = os.path.basename(prg)[:-4]
        regmatch = prefix.match(prgname)
        if regmatch:
            name = regmatch.group(3)
            menuid = regmatch.group(1)
        else:
            name = prgname
            menuid = '1'
        regmatch = suffix.match(name)
        if regmatch:
            name = regmatch.group(1)
            run = regmatch.group(2)
            if run[:2] == "0x":
                run = int(run, 16)
            else:
                run = int(run)
        else:
            run = 0
        print("    Menu %s: %s" % (menuid,name))
        name = name.replace("_"," ")
        if not menuid in menus:
            menus[menuid] = genmenu(CompConfig['menu'])
        menus[menuid]['items'].append(genitem(name,prgname,run))

if len(programs) > 0:
    print("\nFollowing program sections were unused in cartridge:")
    for prgsec in sorted(programs):
        print("    [%s] %s" % (prgsec, cfg.get(prgsec,"file")) )

#############################################################
#           Getting information on all PRG files
#############################################################

#list of prg names, used to detect repeated programs
load_list = []

#printmenus()

#load all programs
names_len = 0
for menuid in sorted(menus):
    menu = menus[menuid]
    menu['items'].append(genitem("Basic","",CompConfig['basic']))
    items_no = 0
    for i,item in enumerate(menu['items']):
        if item['prg'] == "": continue

        #check if the same file was already loaded
        found = None
        for ii,ll in enumerate(load_list):
            if item['prg'] == ll:
                found = ii
                break
        item['link'] = found

        prgfile=os.path.join(prg_directory,item['prg'])
        if os.access(prgfile+".PRG", os.F_OK):
            prgfile += ".PRG"
        elif os.access(prgfile+".prg", os.F_OK):
            prgfile += ".prg"
        else:
            error("PRG file '%s' not found!" % prgfile)
        prg = open(prgfile,"rb")
        temp = array.array('B')
        temp.fromfile(prg,flen(prg))
        addr = temp.pop(0) + 256*temp.pop(0)
        if ((CompConfig == C128Config) or (CompConfig == C12832KConfig)) and (addr == 0x4001):
            addr = 0x1C01
        item['load'] = addr
        if item['run'] == 0:    # if there is no run address
            item['run'] = addr
        item['len'] = temp.buffer_info()[1]
        item['data'] = temp
        items_no += 1
        load_list.append(item['prg'])
    if items_no > MAX_MENU_ITEMS:
        error("Cartridge menu %s can have max %d programs, but %d supplied."
              % (menuid, MAX_MENU_ITEMS, items_no))

    width = menu['width']
    if width == 0:
        width = min( max( [ len(x['name'])+3 for x in menu['items'] ] ), 34 )
    else:
        width = min(menu['width'],34)
    menu['width'] = width

    height = menu['height']
    cheight = min( 20 if menu['spacing'] == 0 else 10, len(menu['items']) )
    if height == 0:
        height = cheight
    else:
        height = min(menu['height'],20 if menu['spacing'] == 0 else 10, cheight)
    menu['height'] = height

    x = menu['x']
    if x == 0:
        x = (40 - width - 3 - 3) // 2 + 1  #3 for borders, 3 for key display
    else:
        x = max(menu['x'],1)
    menu['x'] = x

    y = menu['y']
    if y == 0:
        y = (25 - height - menu['spacing']*(height-1) - 3) // 2      #3 for borders
    else:
        y = max(menu['y'],1)
    menu['y'] = y

    menu['offset'] = x + 40*y
    #length of all menu names
    nameslen = sum(len(x['name'])+1 for x in menu['items']) #+1 for null character
    menu['itemsoffset'] = names_len
    names_len += nameslen

#############################################################
#           Configuration file generation
#############################################################

#colors
color_border = CompConfig['color_border']
color_background = CompConfig['color_background']
color_character = CompConfig['color_character']

if generate_cfg:
    with open(cfgfile, "w") as cfg:
        # write main configuration
        cfg.write("[cartridge]\n")
        cfg.write("computer=%s\n" % CompConfig['computer'])
        cfg.write("bin=%s\n" % prg_directory)
        cfg.write("prgdir=%s\n" % prg_directory)
        cfg.write("help=%s\n" % CompConfig['help'])
        cfg.write("wave=1\n")
        if cart_sound == 2:
            cfg.write("sound=2\n")
            cfg.write("music=%s\n" % os.path.basename(cart_sid_file))
            cfg.write("song=0\n")
        else:
            cfg.write("sound=1\n")

        # write menu configurations
        for menuid in sorted(menus):
            # write menu N settings
            menu = menus[menuid]
            cfg.write("\n[menu%s]\n" % menuid)
            cfg.write("title=%s %s\n" % (prg_directory,menuid))
            cfg.write("spacing=0\n")
            # write menu N items
            for i,item in enumerate(menu['items']):
                if item['run'] == CompConfig['basic']: continue  # skip BASIC
                cfg.write("\n[prg%s%02d]\n" % (menuid, i))
                cfg.write("file=%s\n" % item['prg'])
                cfg.write("name=%s\n" % item['name'])
                if item['run'] != CompConfig['run']:
                    cfg.write("run=$%04X\n" % item['run'])
    print("\nConiguration '%s' is generated for %s.\n" % (cfgfile, CompConfig['computer']))
    exit(0)

#printmenus()

#############################################################
#           Getting main menu code and music files
#############################################################

print("\nAssembling cartridge file ...")
#open cart menu file
if not os.access(cart_menu_file, os.F_OK):
    if os.access(cart_menu_file+".PRG", os.F_OK):
        cart_menu_file += ".PRG"
    else:
        if os.access(cart_menu_file+".prg", os.F_OK):
            cart_menu_file += ".prg"
        else:
            error("cartridge menu program '%s' not found." % cart_menu_file)
cart_prg = open(cart_menu_file,"rb")
#skip start address
cart_prg.seek(2)
cart_file = array.array('B')
cart_file.fromfile(cart_prg,flen(cart_prg)-2)
cart_prg.close()

#open GoatTracker SID if available
cart_sid = array.array('B')
if cart_sound == 2:
    if cart_sid_file != "":
        if not os.access(cart_sid_file, os.F_OK):
            if os.access(cart_sid_file+CompConfig['musicext'].upper(), os.F_OK):
                cart_sid_file += CompConfig['musicext'].upper()
            elif os.access(cart_sid_file+CompConfig['musicext'], os.F_OK):
                    cart_sid_file += CompConfig['musicext']
            else:
                error("Music file '%s' not found." % cart_sid_file)
        cart_prg = open(cart_sid_file,"rb")
        if cart_sid_file[-4:].lower() == ".sid":
            #skip first 126 bytes
            print("SID")
            cart_prg.seek(126)
            cart_sid.fromfile(cart_prg,flen(cart_prg)-126)
        else:
            #skip first 2 bytes (load address)
            print("BIN",cart_sid_file[:-4])
            cart_prg.seek(2)
            cart_sid.fromfile(cart_prg,flen(cart_prg)-2)
        cart_prg.close()
    else:
        print("Warning: music file not defined, turning sound off")
        cart_sound = 0
sidlen = cart_sid.buffer_info()[1]
cart_sid_pages = int(sidlen / 256)
if sidlen % 256 != 0: cart_sid_pages += 1

#############################################################
#       Assembling of the cartridge binary file
#############################################################

#number of menus
menus_no = len(menus)
# SID = 4, program table = 2, colours = 5, menu data = 8*(1+2+1+1+1+2+2), help = 40
menunamesoffset = 4+2+5+8*(1+2+1+1+1+2+2)+40
menuitemsoffset = menunamesoffset + menus_no*40
#from ProgramTable to last menu item text
menudatasize = menuitemsoffset + names_len

#9 bytes for prgtable per item (10 for Gmod3)
tblsize = (CompConfig['bank_bytes']+8)*sum( [ len(menus[menuid]['items']) for menuid in menus ] )

#calculate table addresses
table_data = array.array('B')
bank = 0
menuprglen = cart_file.buffer_info()[1]
#address of first program inside cartridge memory, starting at 0
crtaddress = menuprglen + menudatasize + tblsize + sidlen
#address of first program inside C64 memory, starting at 0x8000
address = crtaddress + 0x8000
#addres of sid music
sidaddress = address - sidlen
#start address of program table inside C64 memory
tbladdress =  menuprglen + menudatasize + 0x8000
menunamesaddress = menuprglen + menunamesoffset + 0x8000
menuitemsaddress = menunamesaddress + menus_no*40

if crtaddress > 0x2000:
    error("Program data has %d bytes, %d is the maximum.\nShorten program names or number of programs." % (crtaddress, 0x2000) )

#list of program table entries, used for repeated programs
tbl_list = []

print("\nCartridge memory map:\n%31s located at $%06x" % (cart_menu_file, 0))
print("%31s located at $%06x" % ("menu data",menuprglen))
print("%31s located at $%06x" % ("program table",tbladdress-0x8000))
if cart_sid_file != "":
    print("%31s located at $%06x" % ("music",sidaddress-0x8000))
print("\n")
for menuid in sorted(menus):
    menu = menus[menuid]
    for i,item in enumerate(menu['items']):
        prg_data = array.array('B')
        if item['link'] != None:
            print("%31s linked to previous instance" % item['name'])
            prg_data = tbl_list[item['link']]
            table_data.extend(prg_data)
            tbl_list.append(prg_data)
        else:
            if CompConfig['bank_bytes'] == 1:
                append_byte(prg_data,bank)              #bank, 1 byte
            else:
                append_word(prg_data,bank)              #bank, 2 bytes
            append_word(prg_data,address)           #address in bank, 2 bytes
            if item['prg'] != "":                   #length, 2 bytes
                print("%31s located at $%06x ($%06x), Bank %d (%x), run address:" % (item['name'], crtaddress, address, bank, bank), end=" ")
                length = item['data'].buffer_info()[1]
            else:
                length = 0
            append_word(prg_data,length)
            append_word(prg_data,item['load'])      #load address, 2 bytes
            if item['run'] == CompConfig['run']:    #run address, 2 bytes (0 - BASIC RUN)
                append_word(prg_data,0)
                print("BASIC RUN")
            else:
                append_word(prg_data,item['run'])
                if item['prg'] != "":
                    print("$%04x" % item['run'])
            table_data.extend(prg_data)
            tbl_list.append(prg_data)
            address += length                       #next program address
            crtaddress += length
            if CompConfig==C12832KConfig:
                crtaddress += 256 * int(length/CompConfig['romsize']) # add one extra page for every bank to skip over $FF00
            while address > CompConfig['romlast']:
#                print("...address $%06x larger than $%04x, removing $%04x" % (address,CompConfig['romlast'],CompConfig['romsize']))
                address -= CompConfig['romsize']
                bank += 1
 #               print("...address $%06x bank $%02x" % (address,bank))



#assemble cartridge
#for details about various fields in here, check C64 assembler source
append_word(cart_file,sidaddress)           #sid music address
append_byte(cart_file,cart_sid_pages)       #sid number of pages for copying
append_byte(cart_file,cart_sid_song)        #sid song number

append_word(cart_file,tbladdress)           #program table address
append_byte(cart_file,color_border)         #border color
append_byte(cart_file,color_background)     #background color
append_byte(cart_file,color_character)      #character color
append_byte(cart_file,cart_wave)            #menu waving
append_byte(cart_file,cart_sound)           #menu sound

for menuid in sorted(menus):                #menu_items_no
    append_byte(cart_file,len(menus[menuid]['items']))
for i in range(8-len(menus)): append_byte(cart_file,0)

for menuid in sorted(menus):                #menu_offset
    append_word(cart_file,menus[menuid]['offset'])
for i in range(8-len(menus)): append_word(cart_file,0)

for menuid in sorted(menus):                #menu_width
    append_byte(cart_file,menus[menuid]['width'])
for i in range(8-len(menus)): append_byte(cart_file,0)

for menuid in sorted(menus):                #menu_height
    append_byte(cart_file,menus[menuid]['height'])
for i in range(8-len(menus)): append_byte(cart_file,0)

for menuid in sorted(menus):                #menu_spacing
    append_byte(cart_file,menus[menuid]['spacing'])
for i in range(8-len(menus)): append_byte(cart_file,0)

for idx, menuid in enumerate(sorted(menus)):    #menu_names
    append_word(cart_file,menunamesaddress + 40*idx)
for i in range(8-len(menus)): append_word(cart_file,0)

for menuid in sorted(menus):                #menu_items
    append_word(cart_file,menuitemsaddress+menus[menuid]['itemsoffset'])
for i in range(8-len(menus)): append_word(cart_file,0)

append_text(cart_file, center_text(cart_help))

for menuid in sorted(menus):
    append_text(cart_file, menus[menuid]['title'])

for menuid in sorted(menus):
    for item in menus[menuid]['items']:
        append_text(cart_file, item['name'], True)

cart_file.extend(table_data)
cart_file.extend(cart_sid)

for menuid in sorted(menus):
    for item in menus[menuid]['items']:
        if (item['prg'] == "") or (item['link']): continue
        cart_file.extend(item['data'])

if CompConfig==C12832KConfig:
    length = cart_file.buffer_info()[1]
    print("length before C128 32K padding - insert $%02x pages to skip every $7F ($FF00) page - $%06x" % (bank,length))
    new_cart_file = array.array('B')
    old_addr = 0
    while old_addr <= length:
        print(".",end="")
        new_cart_file.extend(cart_file[old_addr:(old_addr+CompConfig['romsize'])])
        new_cart_file.extend([0]*256)
        old_addr += CompConfig['romsize']
    cart_file = new_cart_file
    length = cart_file.buffer_info()[1]
    print("\nnew length after C128 32K padding - $%06x" % (length))

length = cart_file.buffer_info()[1]

#calculate size if set to 0
if cart_sizek == 0:
    for cart_sizek in CompConfig['cartsizes']:
        if (length <= cart_sizek*1024):
            break

#if cart_sizek == 0:
#    cart_sizek = 64
#    while (cart_sizek < 1024) and (length > cart_sizek*1024):
#        cart_sizek *= 2

cart_size = cart_sizek*1024
if length > cart_size:
    larger = length - cart_size
    error("\nCartridge is %d bytes (%d blocks) larger than it could be."
          % (larger, larger // 254))

#pad to cartridge size
print("\nCartridge size %dk" % cart_sizek, end=" ")
if length < cart_size:
    unused = cart_size - length
    temp = array.array('B',[0xff]*unused)
    cart_file.extend(temp)
    print(", unused %d bytes / %d block(s)" % (unused , unused // 254))
else:
    print("")

#write cartridge to file
cartridge = open(cart_file_name,"wb")
cart_file.tofile(cartridge)
cartridge.close()
print("\nDone! Cartridge saved as '{0}'".format(cart_file_name))
if CompConfig == C64Config:
    print("\nIf needed, you can convert it to Magic Desk crt with cartconv from VICE:")
    print("    cartconv -t md -i '%s' -o '%s.crt'" % (cart_file_name,cart_file_name[:-4]) )
    print("and run it in VICE with:")
    print("    x64sc -cartcrt '%s.crt'\n" % cart_file_name[:-4])
if CompConfig == C64GM3Config:
    print("\nIf needed, you can convert it to Gmod3 crt with cartconv from VICE:")
    print("    cartconv -t gmod3 -i '%s' -o '%s.crt'" % (cart_file_name,cart_file_name[:-4]) )
    print("and run it in VICE with:")
    print("    x64sc -cartcrt '%s.crt'\n" % cart_file_name[:-4])
elif CompConfig == Plus4Config:
    print("\nIf needed, you can convert it to Magic 264 crt with cartconv from VICE:")
    print("    cartconv -t magic -i '%s' -o '%s.crt'" % (cart_file_name,cart_file_name[:-4]) )
    print("and run it in VICE with:")
    print("    xplus4 -cartcrt '%s.crt'\n" % cart_file_name[:-4])
elif CompConfig == C128Config:
    print("\nIf needed, you can convert it to Magic Desk 128 crt with cartconv from VICE:")
    print("    cartconv -t md128 -i '%s' -o '%s.crt'" % (cart_file_name,cart_file_name[:-4]) )
    print("and run it in VICE (3.8 or later) with:")
    print("    x128 -cartcrt '%s.crt'\n" % cart_file_name[:-4])
else:
    print("\nC128 32k banking is not yet supported by emulators, can only be tested with real hardware")

