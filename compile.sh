#!/bin/bash

OPT=$1
#X64=x64sc
X64=/home/zzarko/Compile/VICE-SVN/vice/src/x64
#XPLUS4=xplus4
XPLUS4=/home/zzarko/Compile/VICE-SVN/vice/src/xplus4
X128=/home/zzarko/Compile/VICE-SVN/vice/src/x128
#X128=x128
CARTRIDGE=compilation

if [ "$OPT" == "c" ]; then
    EMULATOR=$X64
    ASM=menu-c64
    OUTPUT=menu-c64
    DEFINE=""
    SWITCH="-c"
elif [ "$OPT" == "3" ]; then
    EMULATOR=$X64
    ASM=menu-c64
    OUTPUT=menu-c64-gm3
    DEFINE="-define GMOD3"
    SWITCH="-3"
elif [ "$OPT" == "p" ]; then
    EMULATOR=$XPLUS4
    ASM=menu-plus4
    OUTPUT=menu-plus4
    DEFINE=""
    SWITCH="-p"
elif [ "$OPT" == "8" ]; then
    EMULATOR=$X128
    ASM=menu-c128
    OUTPUT=menu-c128
    DEFINE=""
    SWITCH="-8"
elif [ -f "$OPT.cfg" ]; then
    CARTRIDGE=$OPT
    computer=$(grep "computer=" "$OPT.cfg" | grep -o c64)
    if [ "$computer" == "c64" ]; then
        OPT="c"
        EMULATOR=$X64
        ASM=menu-c64
    else
        OPT="p"
        EMULATOR=$XPLUS4
        ASM=menu-plus4
    fi
    SWITCH=""
else
    echo "Add option c for C64, 8 for C128 or p for Plus/4 or configuration name"
    exit 1
fi

#BREAK="startCopy"
#BREAK="find_menu_item"
#BREAK="draw_menu_items"
#BREAK="music_init"

#BREAK="draw_menu_item_line"
#BREAK="sidInit"
BREAK="dm0"
#BREAK="pstart"
#BREAK=""

rm -f "$CARTRIDGE.bin" "$CARTRIDGE.crt" 2>/dev/null
java -jar KickAss.jar $DEFINE -o $OUTPUT.prg $ASM.asm
if [ $? -gt 0 ]; then exit 1; fi
./crtgen.py $SWITCH #$CARTRIDGE #1>/dev/null
if [ $? -gt 0 ]; then exit 1; fi

INITBREAK=""
if [ "$BREAK" != "" ]; then
    hexaddr=$(grep -E "[^a-zA-Z]$BREAK[^a-zA-Z0-9_]" "${ASM}.sym" | grep -o -E "[$][a-fA-F0-9]+" | grep -o -E "[a-fA-F0-9]+")
    decaddr=$(printf "%d" $((16#$hexaddr)))
    INITBREAK="-initbreak $decaddr"
    echo "BREAK at: $hexaddr"
fi

echo "Run emulator: $EMULATOR"
if [ "$OPT" == "c" ]; then
    cartconv -t md -i "$CARTRIDGE.bin" -o "$CARTRIDGE.crt"
    if [ $? -gt 0 ]; then exit 1; fi
    $EMULATOR $INITBREAK -cartcrt "$CARTRIDGE.crt" z01b.d64 1>/dev/null 2>/dev/null
elif [ "$OPT" == "3" ]; then
    cartconv -t gmod3 -i "$CARTRIDGE.bin" -o "$CARTRIDGE.crt"
    if [ $? -gt 0 ]; then exit 1; fi
    $EMULATOR $INITBREAK -cartcrt "$CARTRIDGE.crt" 1>/dev/null 2>/dev/null
elif [ "$OPT" == "p" ]; then
    $EMULATOR $INITBREAK -cartmagic "$CARTRIDGE.bin" -8 ./demo/chaos.d64 #1>/dev/null 2>/dev/null
elif [ "$OPT" == "8" ]; then
    #$EMULATOR $INITBREAK -extfunc 1 -extfrom "$CARTRIDGE.bin" 1>/dev/null 2>/dev/null & #-8 ./demo/chaos.d64 #1>/dev/null 2>/dev/null
    $EMULATOR $INITBREAK -drive8type 0 -extfunc 1 -extfrom "$CARTRIDGE.bin" 1>/dev/null 2>/dev/null & #-8 ./demo/chaos.d64 #1>/dev/null 2>/dev/null
    # place VICE window in upper left corner
    sleep 0.7
    wmctrl -r :ACTIVE: -e 0,0,0,-1,-1
fi

